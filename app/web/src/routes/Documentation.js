import React, {useState} from 'react';

export default function Documentation() {
    const [docFile, setDocFile] = useState('');
    // fetching from folder 'public/' as a normal request
    fetch('static/doc/README.md.html')
        .then(r => r.text())
        .then(setDocFile);

    return (
        <div dangerouslySetInnerHTML={{__html: docFile}}></div>
    );
}
