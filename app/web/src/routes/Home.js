/**
 * *********
 * Shortcuts
 * *********
 * - PS = Pseudonymization
 */

import React, {Component} from 'react';
import {Col, Form, Row, Accordion} from 'react-bootstrap';

import WordPopover from '../components/WordPopover';
import SettingsSidebar from '../components/SettingsSidebar';
import {deserialize, pseudonymize} from '../services/application';
import {modes} from '../globals';
import Notification from '../components/Notification';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {faCircleInfo} from '@fortawesome/free-solid-svg-icons';

export default class Home extends Component {
    constructor(props) {
        super(props);

        // supported named entity types
        this.keysPS = [
            'T',
            'pf', 'ps', 'pm',
            'gu', 'gs', 'gh', 'gu', 'gc',
            'ty',
            'na', 'no',
            'if', 'ic',
            'o',
            'mn'
        ];

        this.selectedKeysPS = [];

        this.state = {
            blackMode: false,
            mode: modes.INPUT,
            errorShown: false,
            errorMessage: ''
        };
    }

    collectOptions = () => {
        const selectedKeysPS = this.keysPS.filter(
            keyPS => document.getElementById(keyPS).checked
        );

        if (selectedKeysPS.includes('pf') && selectedKeysPS.includes('ps')) {
            selectedKeysPS.push('P');
        }

        if (selectedKeysPS.includes('o')) {
            selectedKeysPS.push(/*'o',*/ 'o_', 'oa', 'op', 'or');
        }

        let randomOtherEntitiesOption = document.getElementById('other-entities-random-option').checked;
        let staticOtherEntitiesOption = document.getElementById('other-entities-static-option').checked;

        let otherOption = 'static';
        if (randomOtherEntitiesOption) {
            otherOption = 'random';
        } else if (staticOtherEntitiesOption) {
            otherOption = 'static';
        }

        let randomPersonalOption = document.getElementById('randomize-personal-names').checked;
        let contextPersonalOption = document.getElementById('context-personal-names').checked;
        let staticPersonalOption = document.getElementById('static-personal-names').checked;

        let personalOption = 'static';
        if (randomPersonalOption) {
            personalOption = 'random';
        } else if (contextPersonalOption) {
            personalOption = 'context';
        } else if (staticPersonalOption) {
            personalOption = 'static';
        }

        this.userInputValue = document.getElementById('user-text-input').value;
        let nametagVersion = 2;
        nametagVersion = document.getElementById('nametag-2').checked && 2;
        nametagVersion = document.getElementById('nametag-1').checked && 1;

        let language = false;
        if (document.getElementById('lang-cs').checked) {
            language = 'cs';
        } else if (document.getElementById('lang-en').checked) {
            language = 'en';
        }
        
        return {
            text: this.userInputValue,
            personal_name_mode : personalOption,
            other_mode: otherOption,
            split_paragraphs:true,
            nametag_version: nametagVersion,
            language: language,
            selectedKeysPS: selectedKeysPS
        };
    };

    /**
     * User clicked on the 'Preview' button.
     * @returns {Promise<void>}
     */
    makePreview = async () => {
        const {selectedKeysPS, ...options} = this.collectOptions();
        this.PSResponse = await pseudonymize(options, selectedKeysPS);

        if (Object.prototype.hasOwnProperty.call(this.PSResponse, 'error_message')) {
            this.setState({
                errorMessage: this.PSResponse['error_message'],
                errorShown: true
            });
        } else {
            this.setState({
                mode: modes.PREVIEW
            });
        }
    };

    /**
     * User clicked 'Back' button.
     */
    makeBack = () => {
        this.setState({
            mode: modes.INPUT
        }, () => {
            document.getElementById('user-text-input').value = this.userInputValue;
        });
    };

    /**
     * User clicked 'Detokenize' button.
     * @returns {Promise<void>}
     */
    makeDetokenize = async () => {
        // deserialize on server-side
        this.detokenizeResponse = await deserialize(this.PSResponse);

        this.setState({
            mode: modes.DETOKENIZE
        });
    };

    changeToken = (tokenIdx, newValue) => {
        for (let item of this.PSResponse) {
            // container
            if (Object.prototype.hasOwnProperty.call(item, 'entities')) {
                if (tokenIdx - item['entities'].length + 1 > 0) {
                    tokenIdx -= item['entities'].length;
                    continue;
                }
            // simple token
            } else if (tokenIdx > 0) {
                tokenIdx -= 1;
                continue;
            }

            if (Object.prototype.hasOwnProperty.call(item, 'entities')) {
                if (Object.prototype.hasOwnProperty.call(item['entities'][tokenIdx], 'recommended_replace')) {
                    item['entities'][tokenIdx]['recommended_replace'] = newValue;
                } else {
                    item['entities'][tokenIdx]['form'] = newValue;
                }
            } else {
                if (Object.prototype.hasOwnProperty.call(item, 'recommended_replace')) {
                    item['recommended_replace'] = newValue;
                } else {
                    item['form'] = newValue;
                }
            }

            break;
        }
    };

    /**
     * Gets Popover element for a word.
     * @param orig original word
     * @param PSd pseudonymized word
     * @param wasPSed true if the given word was pseudonymized, false otherwise
     * @param index index of the token (indexing from 0)
     * @param preWhitespaces
     * @returns {JSX.Element}
     */
    getPopoverElement(orig, PSd, wasPSed, index, preWhitespaces) {
        return <WordPopover
            orig={orig}
            PSd={PSd}
            wasPSed={wasPSed}
            index={index}
            changeHandler={this.changeToken}
            preWhitespaces={preWhitespaces}
            blackMode={this.state.blackMode} />;
    }

    createContainerChildRepresentation(ent, idx) {
        if (Object.prototype.hasOwnProperty.call(ent, 'recommended_replace')) {
            if (ent.recommended_replace !== '') {
                return <span key={idx}>{this.getPopoverElement(ent.form, ent.recommended_replace, true, idx, ent.pre_whitespaces)}</span>;
            } else {
                return (
                    <span key={'white-space-container-' + idx}>
                        {ent.pre_whitespaces}
                        <s key={idx} className={this.state.blackMode && 'black-mode'}>
                            {ent.form}
                        </s>
                    </span>
                );
            }
        } else {
            return <span key={idx}>{this.getPopoverElement(ent.form, '', false, idx, ent.pre_whitespaces)}</span>;
        }
    }

    createItemRepresentation(cur, index) {
        // simple named entity (i.e. named entity without container)
        if (Object.prototype.hasOwnProperty.call(cur, 'recommended_replace')) {
            if (cur.recommended_replace !== '') {
                return (
                    <span key={'token-' + index}>
                        <span>{cur.pre_whitespaces.toHtmlEntities}</span>
                        {this.getPopoverElement(cur.form, cur.recommended_replace, true, index, cur.pre_whitespaces)}
                    </span>
                );
            } else {
                return (
                    <span key={'white-space-container-' + index} className={this.state.blackMode && 'black-mode'}>
                        {cur.pre_whitespaces}
                        <s key={index}>
                            {cur.form}
                        </s>
                    </span>
                );
            }
        }
        // container
        else if (Object.prototype.hasOwnProperty.call(cur, 'entities')) {
            return (
                <span key={'token-' + index}>
                    {
                        cur.entities.map((ent, containerChildIdx) => {
                            return this.createContainerChildRepresentation(ent, index + containerChildIdx);
                        })
                    }
                </span>
            );
        }
        // normal word, i.e. not named entity OR it is named entity, but user does not want to pseudonymize it
        else {
            return (
                <span key={'token-' + index}>
                    {this.getPopoverElement(cur.form, cur.form, false, index, cur.pre_whitespaces)}
                </span>
            );
        }
    }

    render() {
        let cumulativeIdx = 0;

        let resultView;
        // INPUT mode
        if (this.state.mode === modes.INPUT) {
            resultView = (
                <Form.Group as={Col} xs={12} sm={12} md={7} lg={8} key={'main-view-input'} className="mb-3">
                    <Form.Control id={'user-text-input'} as="textarea" placeholder={'Enter text for pseudonymization'} rows={27}/>
                </Form.Group>
            );
        }
        // PREVIEW mode
        else if (this.state.mode === modes.PREVIEW) {
            resultView = (
                <Form.Group as={Col} xs={12} sm={12} md={7} lg={8} className="mb-3" id={'preview-container'} key={'main-view-preview'}>
                    <div className={'jumbotron-container'}>
                        {
                            this.PSResponse.map((cur) => {
                                const representation = this.createItemRepresentation(cur, cumulativeIdx);

                                if (Object.prototype.hasOwnProperty.call(cur, 'entities')) {
                                    cumulativeIdx += cur['entities'].length;
                                } else {
                                    cumulativeIdx += 1;
                                }

                                return representation;
                            })
                        }
                    </div>
                </Form.Group>
            );
        // DETOKENIZE mode
        } else if (this.state.mode === modes.DETOKENIZE) {
            resultView = (
                <Form.Group as={Col} xs={12} sm={12} md={7} lg={8} className="mb-3" id={'detokenize-container'} key={'main-view-detokenize'}>
                    <div className={'jumbotron-container'}>
                        <div style={{whiteSpace: 'pre-wrap'}}>
                            {this.detokenizeResponse}
                        </div>
                    </div>
                </Form.Group>
            );
        }

        return (
            <>
                {
                    this.state.errorShown &&
                        <Notification
                            header={'Error'}
                            body={this.state.errorMessage}
                            onClose={() => this.setState({
                                errorShown: false
                            })}
                        />
                }
                <Accordion style={{marginBottom: '15px'}}>
                    <Accordion.Item eventKey="0">
                        <Accordion.Header>
                            <FontAwesomeIcon icon={faCircleInfo} style={{marginRight: '5px'}} /> Guide
                        </Accordion.Header>
                        <Accordion.Body>
                            <ol>
                                <li>on top of the textarea, select pseudonymization options,</li>
                                <li>on the right side of the textarea, select named entities subject to pseudonymization,</li>
                                <li>enter the text you want to pseudonymize into the textarea,</li>
                                <li>after you are done with step 1., 2., and 3., click the &apos;Preview&apos; button in the bottom right.</li>
                            </ol>
                        </Accordion.Body>
                    </Accordion.Item>
                </Accordion>

                <Form>
                    <Row>
                        <Row>
                            <h4>Pseudonymization options</h4>
                            <Col sm={12} md={2}>
                                <h5>NameTag version</h5>
                                <Form.Group className="mb-2">
                                    <Form.Check
                                        type="radio"
                                        id="nametag-2"
                                        label="NameTag 2"
                                        name={'nametag-version'}
                                        disabled={this.state.mode !== modes.INPUT}
                                    />
                                    <Form.Check
                                        type="radio"
                                        defaultChecked={true}
                                        id="nametag-1"
                                        label="NameTag 1"
                                        name={'nametag-version'}
                                        disabled={this.state.mode !== modes.INPUT}
                                    />
                                </Form.Group>
                            </Col>
                            <Col sm={12} md={2}>
                                <h5>Language</h5>
                                <Form.Group className="mb-2">
                                    <Form.Check
                                        type="radio"
                                        defaultChecked={true}
                                        id="lang-cs"
                                        label="Czech"
                                        name={'language'}
                                        disabled={this.state.mode !== modes.INPUT}
                                    />
                                    <Form.Check
                                        type="radio"
                                        id="lang-en"
                                        label="English"
                                        name={'language'}
                                        disabled={this.state.mode !== modes.INPUT}
                                    />
                                </Form.Group>
                            </Col>
                            <Col sm={12} md={4}>
                                <Row>
                                    <h5>Black mode</h5>
                                    <Form.Group className="mb-2">
                                        <Form.Check
                                            label='Black mode'
                                            aria-label="Black mode"
                                            defaultChecked={false}
                                            id={'black-mode'}
                                            name={'black-mode'}
                                            disabled={this.state.mode !== modes.INPUT}
                                            onClick={() => this.setState({blackMode: !this.state.blackMode})}
                                        />
                                    </Form.Group>
                                </Row>
                                <Row>
                                    <Col sm={12} md={6}>
                                        <h5>Personal names</h5>
                                        <Form.Group className="mb-2">
                                            <Form.Check
                                                type='radio'
                                                label="Random"
                                                aria-label="Randomization of personal names"
                                                defaultChecked={true}
                                                id={'randomize-personal-names'}
                                                name={'personal-names-mode'}
                                                disabled={this.state.mode !== modes.INPUT || this.state.blackMode}
                                            />
                                            <Form.Check
                                                type='radio'
                                                label="Context"
                                                aria-label="Context replacing of personal names"
                                                id={'context-personal-names'}
                                                name={'personal-names-mode'}
                                                disabled={this.state.mode !== modes.INPUT || this.state.blackMode}
                                            />
                                            <Form.Check
                                                type='radio'
                                                label="Static"
                                                aria-label="Static replacing of personal names"
                                                id={'static-personal-names'}
                                                name={'personal-names-mode'}
                                                disabled={this.state.mode !== modes.INPUT || this.state.blackMode}
                                            />
                                        </Form.Group>
                                    </Col>
                                    <Col sm={12} md={6}>
                                        <h5>Others</h5>
                                        <Form.Group className="mb-2">
                                            <Form.Check
                                                label="Random"
                                                type='radio'
                                                aria-label='Randomization of other entities'
                                                defaultChecked={true}
                                                id={'other-entities-random-option'}
                                                name={'other-entities-mode'}
                                                disabled={this.state.mode !== modes.INPUT || this.state.blackMode}
                                            />
                                            <Form.Check
                                                label="Static"
                                                type='radio'
                                                aria-label="Static replacing of other entities"
                                                id={'other-entities-static-option'}
                                                name={'other-entities-mode'}
                                                disabled={this.state.mode !== modes.INPUT || this.state.blackMode}
                                            />
                                        </Form.Group>
                                    </Col>
                                </Row>
                            </Col>
                        </Row>

                        {resultView}

                        <SettingsSidebar
                            mode={this.state.mode}
                            makePreview={this.makePreview}
                            makeBack={this.makeBack}
                            makeDetokenize={this.makeDetokenize}
                            blackMode={this.state.blackMode}
                        />
                    </Row>
                </Form>
            </>
        );
    }
}
