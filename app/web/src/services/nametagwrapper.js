import {API_WRAPPER_BASE_URL} from '../globals';

export const make_ner = async (text) => {
    return await fetch(`${API_WRAPPER_BASE_URL}/make_ner/`, {
        method: 'POST',
        body: text
    });
};
