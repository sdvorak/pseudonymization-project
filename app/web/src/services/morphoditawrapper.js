import {API_WRAPPER_BASE_URL} from '../globals';

export const generate_guess = async (form, tag) => {
    return fetch(`${API_WRAPPER_BASE_URL}/generate/${form}/${tag}/guess`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    })
        .then(resp => resp.json());
};

export const generate = async (lemma, tag) => {
    return fetch(`${API_WRAPPER_BASE_URL}/generate/${lemma}/${tag}`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    })
        .then(resp => resp.json());
};

export const lemmatize = async (form) => {
    return fetch(`${API_WRAPPER_BASE_URL}/lemmatize/${form}`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    })
        .then(resp => resp.json());
};

export const analyze = async (text) => {
    return fetch(`${API_WRAPPER_BASE_URL}/analyze`, {
        method: 'POST',
        body: text,
        headers: {
            'Content-Type': 'application/json'
        }
    })
        .then(resp => resp.json());
};

export const tokenize = async (text) => {
    return fetch(`${API_WRAPPER_BASE_URL}/tokenize`, {
        method: 'POST',
        body: text,
        headers: {
            'Content-Type': 'application/json'
        }
    })
        .then(resp => resp.json());
};