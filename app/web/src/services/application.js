import {API_WRAPPER_BASE_URL} from '../globals';

/**
 *
 * @param parameters JSON with parameters in this format:
 *      {
 *          text: text subject to pseudonymization,
 *          personal_name_mode : either string 'context', 'random', or 'static',
 *          other_mode: either string 'random', or 'static',
 *          split_paragraphs: bool,
 *          nametag_version: either integer 1 or 2,
 *      }
 * @param selectedEntities JSON with names of entities to be pseudonymized, e.g.
 *      {
 *          P, pf, ps, ...
 *      }
 * @returns {Promise<any>}
 */
export const pseudonymize = async (parameters, selectedEntities) => {
    let params = new URLSearchParams(parameters);

    for (const entity of selectedEntities) {
        params.append('selected_entities[]', entity);
    }

    return await fetch(`${API_WRAPPER_BASE_URL}/pseudonymize?` + params, {
        method: 'GET',
        headers: {
            'Accept': 'application/json'
        }
    }).then(resp => {
        const contentType = resp.headers.get('content-type');

        // server answered with JSON, but HTTP status code is
        // not a success.
        if (!resp.ok &&
            contentType &&
            contentType.indexOf('application/json') !== -1) {
            return resp.json();
        }

        // probably NetworkError
        if (!resp.ok) {
            return resp.text().then(errorMessage => {
                console.log('here');
                return {
                    error_message: `An error has occurred. Error message is: '${errorMessage}'`
                };
            });
        }

        return resp.json();
    });
};

export const deserialize = async (itemsArrays) => {
    let params = new URLSearchParams({});

    for (const itemArray of itemsArrays) {
        params.append('item_array[]', JSON.stringify(itemArray));
    }

    return await fetch(`${API_WRAPPER_BASE_URL}/deserialize?` + params, {
        method: 'get',
        headers: {
            'Content-Type': 'application/json'
        }
    }).then(resp => resp.text());
};
