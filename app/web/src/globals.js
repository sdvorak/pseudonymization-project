export const APP_NAME = 'Data pseudonymization tool';
export const API_WRAPPER_BASE_URL = 'http://localhost:8080';

export const modes = {INPUT: 1, PREVIEW: 2, DETOKENIZE: 3};
