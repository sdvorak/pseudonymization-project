// Bootstrap
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.min.js';
import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import {useNavigate} from 'react-router-dom';
import React, {useEffect} from 'react';

import {APP_NAME} from '../globals';

export default function Navigation() {
    const navigate = useNavigate();

    useEffect(() => {
        document.title = APP_NAME;
    }, []);

    return (
        <Navbar bg="light" expand="lg" className={'mb-2'}>
            <Container>
                <Navbar.Brand style={{'cursor': 'pointer'}} onClick={() => navigate('/home')}>{APP_NAME}</Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="me-auto">
                        <Nav.Link onClick={() => navigate('/home')}>Home</Nav.Link>
                        <Nav.Link onClick={() => navigate('/doc')}>Documentation</Nav.Link>
                        {/*<Nav.Link onClick={() => navigate('/about')}>About</Nav.Link>*/}
                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    );
}
