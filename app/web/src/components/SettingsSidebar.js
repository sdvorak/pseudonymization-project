import React, {Component} from 'react';
import PropTypes from 'prop-types';

import {Button, Form, Col} from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {faArrowLeft} from '@fortawesome/free-solid-svg-icons';

import {modes} from '../globals';

export default class SettingsSidebar extends Component {
    constructor(props) {
        super(props);
    }
    
    getModeButtons = () => {
        let buttons;
        if (this.props.mode === modes.INPUT) {
            buttons = (
                <Button
                    variant="primary"
                    className="me-1 mb-1"
                    onClick={() => this.props.makePreview()}
                >
                    Preview
                </Button>
            );
        } else if (this.props.mode === modes.PREVIEW) {
            buttons = (
                <>
                    <Button
                        variant="primary"
                        className="me-1 mb-1"
                        onClick={() => this.props.makeBack()}
                    >
                        <FontAwesomeIcon icon={faArrowLeft} />
                        &nbsp;Back
                    </Button>
                    <Button
                        variant="primary"
                        className="me-1 mb-1"
                        onClick={() => this.props.makeDetokenize()}
                        disabled={this.props.blackMode}
                    >
                        Plain text
                    </Button>
                </>
            );
        } else if (this.props.mode === modes.DETOKENIZE) {
            buttons = (
                <Button
                    variant="primary"
                    className="me-1 mb-1"
                    onClick={() => this.props.makeBack()}
                >
                    <FontAwesomeIcon icon={faArrowLeft} />
                    &nbsp;Back
                </Button>
            );
        }
        
        return buttons;
    };

    render() {
        return (
            <Form.Group as={Col} xs={12} sm={12} md={5} lg={4} className="mb-3">
                <h4>Entities to pseudonymize</h4>
                <Form.Group className="mb-2">
                    <h5>Personal names</h5>
                    <div className='check-group'>
                        <Form.Check
                            label="First names"
                            aria-label="Entity for pseudonymization"
                            defaultChecked={true}
                            id={'pf'}
                            disabled={this.props.mode !== modes.INPUT}
                        />
                        <Form.Check
                            label="Middle names"
                            aria-label="Entity for pseudonymization"
                            defaultChecked={true}
                            id={'pm'}
                            disabled={this.props.mode !== modes.INPUT}
                        />
                        <Form.Check
                            label="Second names"
                            aria-label="Entity for pseudonymization"
                            defaultChecked={true}
                            id={'ps'}
                            disabled={this.props.mode !== modes.INPUT}
                        />
                    </div>
                    <h5>Numerals</h5>
                    <div className='check-group'>
                        <Form.Check
                            label="Complex Time Expressions"
                            aria-label="Entity for pseudonymization"
                            defaultChecked={true}
                            id={'T'}
                            disabled={this.props.mode !== modes.INPUT}
                        />
                        <Form.Check
                            label="Years"
                            aria-label="Entity for pseudonymization"
                            defaultChecked={true}
                            id={'ty'}
                            disabled={this.props.mode !== modes.INPUT}
                        />
                        <Form.Check
                            label="Ages"
                            aria-label="Entity for pseudonymization"
                            defaultChecked={true}
                            id={'na'}
                            disabled={this.props.mode !== modes.INPUT}
                        />
                        <Form.Check
                            label="Ordinal numbers"
                            aria-label="Entity for pseudonymization"
                            defaultChecked={true}
                            id={'no'}
                            disabled={this.props.mode !== modes.INPUT}
                        />
                    </div>
                    <h5>Geographicals</h5>
                    <div className='check-group'>
                        <Form.Check
                            label="Streets/Squares"
                            aria-label="Entity for pseudonymization"
                            defaultChecked={true}
                            id={'gs'}
                            disabled={this.props.mode !== modes.INPUT}
                        />
                        <Form.Check
                            label="Cities/Towns"
                            aria-label="Entity for pseudonymization"
                            defaultChecked={true}
                            id={'gu'}
                            disabled={this.props.mode !== modes.INPUT}
                        />
                        <Form.Check
                            label="Countries"
                            aria-label="Entity for pseudonymization"
                            defaultChecked={true}
                            id={'gc'}
                            disabled={this.props.mode !== modes.INPUT}
                        />
                        <Form.Check
                            label="Hydronyms"
                            aria-label="Entity for pseudonymization"
                            defaultChecked={true}
                            id={'gh'}
                            disabled={this.props.mode !== modes.INPUT}
                        />
                    </div>

                    <h5>Artifacts</h5>
                    <div className='check-group'>
                        <Form.Check
                            label="Companies/Concerns, ..."
                            aria-label="Entity for pseudonymization"
                            defaultChecked={true}
                            id={'if'}
                            disabled={this.props.mode !== modes.INPUT}
                        />
                        <Form.Check
                            label="Cultural, Educational, Scientific institutions, ..."
                            aria-label="Entity for pseudonymization"
                            defaultChecked={true}
                            id={'ic'}
                            disabled={this.props.mode !== modes.INPUT}
                        />
                        <Form.Check
                            label="Artifact names (cultural artifacts (books, movies), ...)"
                            aria-label="Entity for pseudonymization"
                            defaultChecked={true}
                            id={'o'}
                            disabled={this.props.mode !== modes.INPUT}
                        />
                        <Form.Check
                            label="Periodicals"
                            aria-label="Entity for pseudonymization"
                            defaultChecked={true}
                            id={'mn'}
                            disabled={this.props.mode !== modes.INPUT}
                        />
                    </div>
                </Form.Group>

                {this.getModeButtons()}
            </Form.Group>
        );
    }
}

SettingsSidebar.propTypes = {
    mode: PropTypes.number,
    makePreview: PropTypes.func,
    makeBack: PropTypes.func,
    makeDetokenize: PropTypes.func,
    blackMode: PropTypes.bool
};
