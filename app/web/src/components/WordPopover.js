import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Button, Form, InputGroup, OverlayTrigger, Popover} from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {faExchange, faUndo} from '@fortawesome/free-solid-svg-icons';

export default class WordPopover extends Component {
    constructor(props) {
        super(props);
    }

    getOverlayedElement = () => {
        let header;
        let revertBtn = (<></>);
        if (this.props.wasPSed) {
            header = `Word '${this.props.orig}' was replaced with '${this.props.PSd}'`;

            revertBtn = (
                <Button variant="danger" key={'revert-btn-' + this.props.index} type="button" onClick={(event) => {
                    document.getElementById('token-'+this.props.index).innerText = this.props.orig;
                    event.target.remove();
                    this.props.changeHandler(this.props.index, this.props.orig);
                }}>
                    <FontAwesomeIcon key={'undo-icon-' + this.props.index} icon={faUndo}/>
                    &nbsp;Revert to &apos;{this.props.orig}&apos;
                </Button>
            );
        } else {
            header = `Actions with word '${this.props.orig}'`;
        }

        return (
            <Popover id={'popover-positioned-top'} key={'popover-' + this.props.index}>
                <Popover.Header as="h3" key={'popover-header-' + this.props.index}>{header}</Popover.Header>
                <Popover.Body key={'popover-body-' + this.props.index}>
                    <InputGroup className={'mb-2'} key={'popover-inputgroup-' + this.props.index}>
                        <Form.Control
                            type="text"
                            placeholder={`Replace ${this.props.PSd || this.props.orig} with`}
                            id={'token-input-' + this.props.index} key={'token-input-' + this.props.index}
                        />
                        <Button key={'popover-inputgroup-btn-' + this.props.index} variant="success" type="button" onClick={() => {
                            const newValue = document.getElementById('token-input-'+this.props.index).value;
                            document.getElementById('token-'+this.props.index).innerText = newValue;
                            this.props.changeHandler(this.props.index, newValue);
                        }}>
                            <FontAwesomeIcon icon={faExchange} key={'popover-exchange-icon-' + this.props.index}/>
                        </Button>
                    </InputGroup>

                    {revertBtn}
                </Popover.Body>
            </Popover>
        );
    };

    /**
     * Returns popover for word that was not pseudonymized.
     * @returns {JSX.Element}
     */
    getPSPopover = () => {
        return (
            <span key={'popover-container-' + this.props.index}>
                <span key={'token-space-' + this.props.index}>{this.props.preWhitespaces}</span>
                {/*rootClose closes popover after clicking outside it*/}
                <OverlayTrigger trigger="click" key={'top-' + this.props.index} placement={'top'} rootClose
                    overlay={
                        this.getOverlayedElement()
                    }>
                    {/* className={this.state.blackMode && 'black-mode'}*/}
                    <span id={'token-' + this.props.index}
                        key={'token-' + this.props.index}
                        className={`${this.props.wasPSed ? 'pseudonymized' : ''} ${(this.props.blackMode && this.props.wasPSed) ? 'black-mode' : ''}`}>
                        {this.props.PSd || this.props.orig}
                    </span>
                </OverlayTrigger>
            </span>
        );
    };

    render() {
        return this.getPSPopover();
    }
}

WordPopover.propTypes = {
    orig: PropTypes.string,
    PSd: PropTypes.string,
    index: PropTypes.number,
    wasPSed: PropTypes.bool,
    changeHandler: PropTypes.func,
    preWhitespaces: PropTypes.string,
    blackMode: PropTypes.bool
};
