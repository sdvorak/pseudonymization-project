import React, {useEffect} from 'react';
import ReactDOM from 'react-dom/client';

// Routing
import {
    BrowserRouter,
    Routes,
    Route,
} from 'react-router-dom';

import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.min.js';

import Container from 'react-bootstrap/Container';

import Navigation from './components/Navigation';
import './index.css';
import Home from './routes/Home';
import About from './routes/About';
import Documentation from './routes/Documentation';
import {APP_NAME} from './globals';

export default function App() {
    useEffect(() => {
        document.title = APP_NAME;
    }, []);

    return (
        <React.StrictMode>
            <BrowserRouter>
                <Navigation/>
                <Container style={{marginTop: '20px'}}>
                    <Routes>
                        <Route path="/" element={<Home/>}/>
                        <Route path="/home" element={<Home/>}/>
                        <Route path="/about" element={<About/>}/>
                        <Route path="/doc" element={<Documentation/>}/>
                    </Routes>
                </Container>
            </BrowserRouter>
        </React.StrictMode>
    );
}

const root = ReactDOM.createRoot(
    document.getElementById('root')
);


root.render(
    <React.StrictMode>
        <App />
    </React.StrictMode>
);
