# Calling API
```js
const result1 = await generate_guess('Karla', 'NNFS3-----A----');
const result2 = await generate('Karla', 'NNFS3-----A----');
const result3 = await lemmatize('Karla');
const result4 = await analyze('Kamarád říkal, že o Karlovi hodně ví.');
const result5 = await tokenize('Kamarád říkal, že o Karlovi hodně ví.');

console.log(result1);
console.log(result2);
console.log(result3);
console.log(result4);
console.log(result5);
```


## npm log

```shell
# Create React App
npx create-react-app web
# Router v6
npm install react-router-dom@6
# Bootstrap
npm install react-bootstrap bootstrap
# FontAwesome
npm i --save @fortawesome/fontawesome-svg-core
npm install --save @fortawesome/free-solid-svg-icons
npm install --save @fortawesome/react-fontawesome
# eslint
npm install eslint --save-dev
npm init @eslint/config
npm install html-loader
```