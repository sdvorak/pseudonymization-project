"""
NameTag 1 Wrappers
"""
import sys
from os.path import exists
from typing import List

from ufal.nametag import Ner, NamedEntities

from entityrecognition import EntityRecognition
import mg


class NameTag1Wrapper(EntityRecognition):
    """
    NameTag 1 Wrappers
    """
    def __init__(self, nametag_model='models/czech-cnec2.0-140304.ner'):
        # do not forget to download the models. Read the instructions in README.md's.
        if not exists(nametag_model):
            print(f'Specified file for NameTag 1 model does not exist.'
                  f'Specified file was: \'{nametag_model}\'')
            sys.exit(1)

        self.ner = Ner.load(nametag_model)

        if not self.ner:
            print(f'Cannot load model for NameTag 1 from file \'{nametag_model}\'')
            sys.exit(1)

    @staticmethod
    def sort_entities(entities: NamedEntities) -> list:
        """
        Sort entities.
        :param entities: entities
        :return: sorted entities by the index of occurrence in the text
        """
        return sorted(entities, key=lambda entity: (entity.start, -entity.length))

    def serialize(self, text: str, lang: str) -> List[dict]:
        """
        Perform named entity recognition in the text.
        :return: array of JSONs that identifies each named entity &
                 other entities (non-named entities, ...)
        """
        from ufal.morphodita import Forms, TokenRanges
        c_forms = Forms()
        tokens = TokenRanges()

        entities = NamedEntities()

        tags = mg.MW.tag_text(text)

        token_end_idx = 0
        sentence_token_offset = 0

        named_entities = []

        # use MorphoDiTa tokenizer
        tokenizer = mg.MW.morpho.newTokenizer()
        tokenizer.setText(text)

        all_tokens = mg.MW.flatten_tokenize(text)

        while tokenizer.nextSentence(c_forms, tokens):
            forms = list(c_forms)
            self.ner.recognize(forms, entities)

            sorted_entities = NameTag1Wrapper.sort_entities(entities)

            named_entity_idx = 0
            while named_entity_idx < len(sorted_entities):
                named_entity = sorted_entities[named_entity_idx]
                token_start_idx = named_entity.start

                # previous token exist (i.e. it is not the first token)
                if sentence_token_offset + token_start_idx - 1 >= 0:
                    last_token_text_end =\
                        all_tokens[sentence_token_offset + token_start_idx - 1]['end']
                else:
                    # it is first token, so the end position of the ''last'' token
                    # is considered as 0
                    last_token_text_end = 0

                token_count = named_entity.length
                token_end_idx = token_start_idx + token_count

                if token_count > 1:
                    container = {
                        'range': {
                            'start': token_start_idx + sentence_token_offset,
                            'end': token_end_idx - 1 + sentence_token_offset
                        },
                        'container_type': named_entity.type,
                        'entities': []
                    }

                    named_entity_idx += 1
                    for tok_idx in range(token_start_idx, token_end_idx):
                        token_text_start_pos = tokens[tok_idx].start
                        token_text_end_pos = token_text_start_pos + tokens[tok_idx].length

                        entity = {'token_idx': tok_idx + sentence_token_offset}
                        if named_entity_idx < len(sorted_entities) and \
                                sorted_entities[named_entity_idx].start == tok_idx:
                            entity['entity_type'] = sorted_entities[named_entity_idx].type
                            entity['form'] = text[token_text_start_pos: token_text_end_pos]
                            entity['tag'] = tags[tok_idx + sentence_token_offset]['tag']
                            entity['lemma'] = tags[tok_idx + sentence_token_offset]['lemma']
                            entity['pre_whitespaces'] =\
                                text[last_token_text_end: token_text_start_pos]
                            container['entities'].append(entity)
                            named_entity_idx += 1
                        else:
                            # inner item does not have a type
                            # (e.g. [Ústí, nad, Labem] -> word 'nad' does not have
                            # a NER type)
                            entity['form'] = text[token_text_start_pos: token_text_end_pos]
                            entity['tag'] = tags[tok_idx + sentence_token_offset]['tag']
                            entity['lemma'] = tags[tok_idx + sentence_token_offset]['lemma']
                            entity['pre_whitespaces'] =\
                                text[last_token_text_end : token_text_start_pos]
                            container['entities'].append(entity)

                        last_token_text_end = token_text_end_pos
                    named_entities.append(container)
                else:
                    token_text_start_pos = tokens[token_start_idx].start
                    token_text_end_pos = token_text_start_pos + tokens[token_start_idx].length

                    entity = {
                        'token_idx': token_start_idx + sentence_token_offset,
                        'entity_type': named_entity.type,
                        'tag': tags[token_start_idx + sentence_token_offset]['tag'],
                        'form': text[token_text_start_pos: token_text_end_pos],
                        'lemma': tags[token_start_idx + sentence_token_offset]['lemma'],
                        'pre_whitespaces': text[last_token_text_end : token_text_start_pos]
                    }

                    named_entities.append(entity)
                    named_entity_idx += 1

            sentence_token_offset += len(tokens)

        response = []

        def get_start_idx(item):
            if 'entities' in item:
                return item['range']['start']

            return item['token_idx']

        def get_end_idx(item):
            if 'entities' in item:
                return item['range']['end']

            return item['token_idx']

        breakpoint = 0
        last_token_text_end = 0
        for ne in named_entities:
            for simple_item_idx in range(breakpoint, get_start_idx(ne)):
                token_text_start_pos = all_tokens[simple_item_idx]['start']
                token_text_end_pos = all_tokens[simple_item_idx]['end']

                response.append({
                    'form': all_tokens[simple_item_idx]['token'],
                    'tag': tags[simple_item_idx]['tag'],
                    'pre_whitespaces': text[last_token_text_end: token_text_start_pos]
                })

                last_token_text_end = token_text_end_pos

            breakpoint = get_end_idx(ne) + 1
            response.append(ne)
            last_token_text_end = all_tokens[breakpoint - 1]['end']

        for simple_item_idx in range(breakpoint, len(all_tokens)):
            token_text_start_pos = all_tokens[simple_item_idx]['start']
            token_text_end_pos = all_tokens[simple_item_idx]['end']

            response.append({
                'form': all_tokens[simple_item_idx]['token'],
                'tag': tags[simple_item_idx]['tag'],
                'pre_whitespaces': text[last_token_text_end: token_text_start_pos]
            })

            last_token_text_end = token_text_end_pos

        return response
