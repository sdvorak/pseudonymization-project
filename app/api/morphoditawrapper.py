"""
MorphoDiTa Wrapper
"""
import sys
from typing import Union, List, Dict, Any
from os.path import exists

from ufal.morphodita import Morpho, Tagger, TaggedLemmas,\
    Forms, TaggedLemmasForms, TokenRanges


class MorphoditaWrapper:
    """
    MorphoDiTa Wrapper
    """
    def __init__(self,
                 morphodita_dict='models/czech-morfflex-161115.dict',
                 morphodita_tagger='models/czech-morfflex-pdt-161115.tagger'
                 ):
        # do not forget to download the models. Read the instructions in README.md's.
        if not exists(morphodita_dict):
            print(f'Specified file for MorphoDiTa Dict does not exist.'
                  f'Specified file was: \'{morphodita_dict}\'')
            sys.exit(1)

        if not exists(morphodita_tagger):
            print(f'Specified file for MorphoDiTa Tagger does not exist.'
                  f'Specified file was: \'{morphodita_tagger}\'')
            sys.exit(1)

        self.morpho = Morpho.load(morphodita_dict)
        self.tagger = Tagger.load(morphodita_tagger)

        if not self.morpho:
            print('Error during loading MorphoDiTa Dict')
            sys.exit(1)

        if not self.tagger:
            print('Error during loading MorphoDiTa Tagger')
            sys.exit(1)

    def lemmatize(self, form: str) -> Union[str, bool]:
        """
        Takes word 'word' on input and returns the 'word's lemma.
        Example: http://127.0.0.1:8080/get_lemma/Honzy
        Result:  {
          "lemmatized": "Honza",
          "word": "Honzy"
        }

        :param form: word to lemmatize
        :return: lemma of the word 'word'
        """
        if len(form.split(' ')) > 1:
            return False

        lemmas = TaggedLemmas()
        forms = Forms([form])

        self.tagger.tag(forms, lemmas)

        return self.morpho.rawLemma(lemmas[0].lemma)

    def generate(self, lemma: str, tag: str) -> Union[str, bool]:
        """
        Example: http://127.0.0.1:8080/lemmatize/Honzy/NNMS3-----A----
        Result:  Honzovi
        :param lemma:
        :param tag:
        :return:
        """
        lemmas_forms = TaggedLemmasForms()
        # Method 'generate' takes lemma!!! So first we need to convert word 'word' to lemma
        lemma_of_word = self.lemmatize(lemma)

        self.morpho.generate(lemma_of_word, tag, self.morpho.GUESSER, lemmas_forms)

        # 'generate' returns 'Struct tagged_lemma_forms' which is 'std::string lemma' and
        # std::vector<tagged_form> forms;
        #
        # So  lemmas_forms                    is vector of 'Struct tagged_lemma_forms'
        #     lemmas_forms[0]                 is first 'Struct tagged_lemma_forms'
        #     lemmas_forms[0].forms           contains forms of the word
        #     lemmas_forms[0].forms[0]        contains first form of the word
        #                                     but it is of type 'Struct tagged_form'
        #                                     which is std::string form & std::string tag;
        #     lemmas_forms[0].forms[0].form   contains actual form

        if len(lemmas_forms) != 1 or len(lemmas_forms[0].forms) != 1:
            return False

        return lemmas_forms[0].forms[0].form

    def generate_guess(self, form: str, tag: str) -> Union[str, bool]:
        """
        Tries to create lemma from the word 'word' and with this lemmatize by tag.
        Example: http://127.0.0.1:8080/lemmatize/Honzy/NNMS3-----A----
        Result:  Honzovi
        :param form:
        :param tag:
        :return:
        """
        lemma_of_word = self.lemmatize(form)
        return self.generate(lemma_of_word, tag)

    def tag_text(self, text: str) -> Union[List[Dict[str, str]], Any]:
        """
        Tag tokens of the text.
        :param text: text to tag
        :return: tagged text
        """
        forms = Forms()
        lemmas = TaggedLemmas()
        tokens = TokenRanges()
        tokenizer = self.tagger.newTokenizer()

        tokenizer.setText(text)
        result = []
        while tokenizer.nextSentence(forms, tokens):
            self.tagger.tag(forms, lemmas)

            for token_struct, lemma_struct in zip(tokens, lemmas):
                lemma = lemma_struct.lemma
                tag = lemma_struct.tag

                result.append({
                    'lemma': self.morpho.rawLemma(lemma),
                    'tag': tag,
                    'token': text[token_struct.start: token_struct.start + token_struct.length]
                })

        return result

    def tokenize(self, text: str) -> List[List[dict]]:
        """
        Tokenize text.
        :param text: text to tokenize
        :return: tokenized text
        """
        forms = Forms()
        lemmas = TaggedLemmas()
        tokens = TokenRanges()
        tokenizer = self.tagger.newTokenizer()

        tokenizer.setText(text)
        result = []
        while tokenizer.nextSentence(forms, tokens):
            self.tagger.tag(forms, lemmas)

            sentence = []
            for i in range(len(lemmas)):
                token = tokens[i]
                sentence.append({
                    'start': token.start,
                    'end': token.start + token.length,
                    'token': text[token.start: token.start + token.length]
                })

            result.append(sentence)

        return result

    def flatten_tokenize(self, text: str) -> List[dict]:
        """
        Tokenize produces list of lists (each representing one sentence).
        Sometimes it is better to have just one list representing the whole text.
        :param text:
        :return:
        """
        tokens = self.tokenize(text)
        return [sentence_token for sentence in tokens for sentence_token in sentence]
