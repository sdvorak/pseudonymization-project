"""
Global variables.
"""

import os

from morphoditawrapper import MorphoditaWrapper
from nametag1wrapper import NameTag1Wrapper
from nametag2wrapper import NameTag2Wrapper


def init() -> None:
    """
    It is necessary to initialize the models once on startup
    (and not again for each request) because it takes a lot of time.

    All the wrappers that uses some model will be accessible using global constants.
    :return:
    """
    global MW
    global NT1
    global NT2

    MW = MorphoditaWrapper(
        morphodita_dict=os.path.dirname(__file__) + '/models/czech-morfflex-161115.dict',
        morphodita_tagger=os.path.dirname(__file__) + '/models/czech-morfflex-pdt-161115.tagger',
    )

    NT1 = NameTag1Wrapper(
        nametag_model=os.path.dirname(__file__) + '/models/czech-cnec2.0-140304.ner'
    )

    # NameTag 2 is used through REST API. So, no model is used.
    NT2 = NameTag2Wrapper()
