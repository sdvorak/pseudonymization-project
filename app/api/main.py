"""
Main module
"""

import json
import logging
from flask import Flask, Response, render_template, request, jsonify
from flask_cors import CORS

import mg
from entityrecognition import EntityRecognition
from ppolicy.policy import Policy
import pseudonymizationpolicystructures as mp

mg.init()

app = Flask(__name__)
CORS(app)

# notice the '!=' sign instead of '==' sign:
if __name__ != '__main__':
    gunicorn_logger = logging.getLogger('gunicorn.error')
    app.logger.handlers = gunicorn_logger.handlers
    app.logger.setLevel(gunicorn_logger.level)


@app.errorhandler(500)
def internal_error(error):
    return Response(
        '{\"error_message\": \"An error has occurred on our side. We will try to fix this issue ASAP.\"}',
        status=500,
        mimetype='application/json'
    )


@app.route('/')
def index():
    """
    Serve a page with API specification GUI presentation.
    :return: page with API specification
    """
    return render_template('index.html')


@app.route('/api-spec')
def get_api_spec():
    """
    Get API specification file. This method is for GUI
    presentation of API specification.
    :return: API specification as string
    """
    with open('api-spec.yaml', mode='r', encoding='utf8') as file:
        return Response(file.read(), mimetype='text/plain')


@app.route('/lemmatize/<form>')
def lemmatize(form: str):
    """
    Get form 'form' and return its lemma. Note that it is ambiguous.
    :param form: word to find lemma of
    :return: lemma of the form 'form'
    """
    lemma = mg.MW.lemmatize(form)
    if isinstance(lemma, bool) and not lemma:
        return Response(
            '{\"error_message\": \"This method expects only one word.\"}',
            status=400,
            mimetype='application/json'
        )

    return {
        'form': form,
        'lemma': lemma
    }


@app.route('/generate/<form>/<tag>/guess')
def generate_guess(form: str, tag: str):
    """
    Gets general form. It does not have to be lemma.
    So first it tries to guess the lemma and then if it succeeds it uses the standard
    method for converting lemma using tag.
    :param form: word to convert to corresponding tag form
    :param tag: target tag
    :return: form with tag 'tag' from 'form'
    """
    generated = mg.MW.generate(form, tag)
    if isinstance(generated, bool) and not generated:
        error_msg = {
            'error_message': 'possible causes:'
                             '\n1. more words were given => give exactly one word.'
                             '\n2. there are more lemmas for the given word => give lemma.'
                             '\n3. something else.'
        }

        return Response(
            json.dumps(error_msg),
            status=400,
            mimetype='application/json'
        )

    return {
        'form': form,
        'generated': generated
    }


@app.route('/generate/<lemma>/<tag>')
def generate(lemma: str, tag: str):
    """
    Generate form with corresponding tag derived from lemma.
    :param lemma: lemma to convert to form with corresponding tag
    :param tag: target tag
    :return: converted 'lemma' to the corresponding form using tag 'tag'
    """
    generated = mg.MW.generate(lemma, tag)
    if isinstance(generated, bool) and not generated:
        error_msg = {
            'error_message': 'possible causes:'
                             '\n1. more words were given => give exactly one word.'
                             '\n2. you did not provide a lemma => give lemma.'
                             '\n3. something else.'
        }

        return Response(
            json.dumps(error_msg),
            status=400,
            mimetype='application/json'
        )

    return {
        'lemma': lemma,
        'generated': generated
    }


@app.route('/analyze', methods=['GET'])
def analyze():
    """
    Performs tagging of the text.
    :return: tagged text
    """
    args = request.args
    text = args.get('text')

    tagged_text = mg.MW.tag_text(text)
    if isinstance(tagged_text, bool) and not tagged_text:
        return Response(
            '{\"error_message\": \"This method expects only one word.\"}',
            status=400,
            mimetype='application/json'
        )

    return jsonify(tagged_text)


@app.route('/tokenize', methods=['GET'])
def tokenize():
    """
    Splits the text into tokens using MorphoDiTa.
    :return: array of tokens
    """
    args = request.args

    text = args.get('text')

    try:
        sentence_splitting =\
            args.get('sentence_splitting', default=True, type=lambda s: s == 'true')
    except ValueError:
        return Response(
            '{\"error_message\":'
            '\"Parameter \'sentence_splitting\' must be of type \'boolean\'.\"}',
            status=400,
            mimetype='application/json'
        )

    if text is None:
        return Response(
            '{\"error_message\": \"Parameter \'text\' is required.\"}',
            status=400,
            mimetype='application/json'
        )

    if sentence_splitting:
        return jsonify(
            list(
                map(
                    lambda sentence_tokens:
                        list(
                            map(
                                lambda token: token['token'],
                                sentence_tokens)
                        ),
                    mg.MW.tokenize(text)
                )
            )
        )

    return jsonify(
        list(
            map(
                lambda token: token['token'],
                mg.MW.flatten_tokenize(text)
            )
        )
    )


@app.route('/ner', methods=['GET'])
def ner():
    """
    Perform named entity recognition in the text.
    :return: array of JSONs that identifies each named entity
    """
    args = request.args

    text = args.get('text')

    if text is None:
        return Response(
            '{\"error_message\": \"Parameter \'text\' is required.\"}',
            status=400,
            mimetype='application/json'
        )

    try:
        nt_version = args.get('nametag_version', type=int, default=2)
    except ValueError:
        return Response(
            '{\"error_message\": \"NameTag version must integer.\"}',
            status=400,
            mimetype='application/json'
        )

    if nt_version not in [1, 2]:
        return Response(
            '{\"error_message\": \"NameTag version must be either \'1\' or \'2\' (int).\"}',
            status=400,
            mimetype='application/json'
        )

    return jsonify(mg.NT1.serialize(text) if nt_version == 1 else mg.NT2.serialize(text))


@app.route('/pseudonymize', methods=['GET'])
def pseudonymize():
    """

    :return:
    """
    args = request.args

    text = args.get('text')

    try:
        selected_entities = args.getlist('selected_entities[]')
    except ValueError:
        return Response(
            '{\"error_message\": \"Entities must be list.\"}',
            status=400,
            mimetype='application/json'
        )

    personal_name_mode = args.get('personal_name_mode')
    other_mode = args.get('other_mode')
    language = args.get('language')

    if language not in ['cs', 'en']:
        return Response(
            '{\"error_message\": \"Language must be either \'cs\' or \'en\'.\"}',
            status=400,
            mimetype='application/json'
        )

    try:
        name_tag_version = args.get('nametag_version', type=int)
    except ValueError:
        return Response(
            '{\"error_message\": \"NameTag version must integer.\"}',
            status=400,
            mimetype='application/json'
        )

    if name_tag_version is None:
        name_tag_version = 2

    if name_tag_version not in [1, 2]:
        return Response(
            '{\"error_message\": \"NameTag version must integer between 1 and 2.\"}',
            status=400,
            mimetype='application/json'
        )

    # text is only required parameter
    if text is None:
        return Response(
            '{\"error_message\": \"Parameter \'text\' is required parameter.\"}',
            status=400,
            mimetype='application/json'
        )

    if personal_name_mode not in ['static', 'context', 'random']:
        return Response(
            '{\"error_message\":'
            '\"Personal name replacing mode must be one of these'
            '[\'static\', \'random\', \'context\'].\"}',
            status=400,
            mimetype='application/json'
        )

    if other_mode not in ['static', 'random']:
        return Response(
            '{\"error_message\":'
            '\"Other replacing mode must be one of these [\'static\', \'random\'].\"}',
            status=400,
            mimetype='application/json'
        )

    result = Policy.pseudonymization(
        text,
        selected_entities,
        language,
        mp.PersonalNameReplacingMode(personal_name_mode),
        mp.OtherReplacingMode(other_mode),
        mg.NT1 if name_tag_version == 1 else mg.NT2
    )

    return jsonify(result)


@app.route('/deserialize', methods=['GET'])
def deserialize():
    """
    Deserialize pseudonymized text.
    :return: Pseudonymized text (string)
    """
    args = request.args

    item_array = args.getlist('item_array[]')

    if item_array is None:
        return Response(
            '{\"error_message\": \"Parameter \'item_array\' is required.\"}',
            status=400,
            mimetype='application/json'
        )

    # convert JSON objects from string to Python dictionary
    item_array_conv = list(map(json.loads, item_array))
    return EntityRecognition.deserialize(item_array_conv)

if __name__ == '__main__':
    # Only for debugging while developing
    app.run(host='0.0.0.0', debug=True, port=8080)
