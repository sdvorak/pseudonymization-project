#!/bin/bash

source_name="$1"
target_name="$1.html"

generate () {
	pandoc -f markdown-raw_html -t html --resource-path=.:templates/ --template toc-template --output=$target_name $source_name --highlight-style=espresso -V colorlinks=true --table-of-contents --filter pandoc-mermaid --lua-filter ./pandocfilters.lua
}

rm -rf mermaid-images

generate
generate

rm -rf ../../web/public/static/doc/mermaid-images/
mv mermaid-images/ ../../web/public/static/doc/
mv "$target_name" ../../web/public/static/doc/
