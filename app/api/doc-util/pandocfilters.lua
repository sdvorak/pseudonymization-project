-- https://stackoverflow.com/questions/48569597/pandoc-filters-change-relative-paths-to-absolute-paths

function fix_path (path)
  return 'static/doc/' .. path
end

-- function Link (element)
--   element.target = fix_path(element.target)
--   return element
-- end

function Image (element)
  element.src = fix_path(element.src)
  return element
end
