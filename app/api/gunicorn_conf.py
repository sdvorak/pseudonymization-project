# Settings manual: https://docs.gunicorn.org/en/stable/settings.html

# probably the highest loglevel (i.e. log everything)
loglevel = "debug"

# "-" is stderr (https://docs.gunicorn.org/en/stable/settings.html#errorlog)
# we want to log to file
# files should be outside shared 'volume', because it is bidirectional
errorlog = "/error.log"

# "-" is stdout (https://docs.gunicorn.org/en/stable/settings.html#logging)
# we want to log to file
# 'accesslog' means logging of HTTP requests
# files should be outside shared 'volume', because it is bidirectional
accesslog = "/access.log"

# user will probably send very long texts, and
# we use GET requests, so everything is in the url.
# For this reason we need to allow very long urls.
# https://docs.gunicorn.org/en/stable/settings.html#limit-request-line
limit_request_line = 8190

worker_tmp_dir = "/dev/shm"
graceful_timeout = 120
timeout = 120
keepalive = 5
threads = 3
