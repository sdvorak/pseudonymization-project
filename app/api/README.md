# REST API
See [REST API documentation](localhost:8080).

# Installation instructions
To install the application you just need to download the models and place them in the correct folder. Rest is done by Docker.

## Download the models
- [MorphoDiTa](https://lindat.mff.cuni.cz/repository/xmlui/handle/11234/1-1836)
    - files needed are `czech-morfflex-161115.dict` and `czech-morfflex-pdt-161115.tagger`
- [NameTag 1](https://lindat.cz/repository/xmlui/handle/11858/00-097C-0000-0023-7D42-8?show=full)
    - file needed is `czech-cnec2.0-140304.ner`

Place them in the `models/` folder.

## Building
*(Using docker/podman)*

All has to be done in a directory with `docker-compose.yml`, i.e. `app/` folder.

### Production build
```bash
docker-compose build --build-arg ENV_TYPE=prod
```

### Development build
```bash
docker-compose build --build-arg ENV_TYPE=dev
```

### Running your build
```bash
docker-compose up
```

#### Notes
- API url `localhost:8080` (you should see SwaggerUI in the browser),
- Web app url `localhost:3000`,
- API access log is in `/access.log` (root directory in the container),
- API error log is in `/error.log` (root directory in the container),
- production build uses `Gunicorn` and development build uses build-in `Werkzeug`.

Reason for not having logs in `/destination_api/...` is because it would mirror the file back inside the repository.

*(Running in development mode in PyCharm)*

- set folder `app/api` as `Source Root`
- click on the green run (play) button left to the `if __name__ == '__main__'`

*(Running in development mode without IDE - e.g. from terminal)*

- make sure you are in Virtual Environment (venv). I.e. run `source venv/bin/activate` (to escape from venv, just write `deactivate`)
- set variable `export PYTHONPATH="${PYTHONPATH}:/path/to/your/project/.../app/api"` [(source)](https://stackoverflow.com/questions/43728431/relative-imports-modulenotfounderror-no-module-named-x#answer-57164633)
- go to `app/api`
- execute `python3 main.py`

# Pseudonymization

## Workflow
```mermaid
flowchart LR
    a["1. Text to pseudonymize (string)"]-->b
    b["2. Serialize (List[dict])"]-->c
    c["3. Pseudonymization (List[dict])"]-->d
    d["4. Deserialize (string)"]
```

### Workflow details

1. user passes text to pseudonymize in the form of string & options,
2. string is tokenized using MorphoDiTa's tokenizer,
3. tokens are passed to NameTag (1/2) in order to recognize named entities:
    - *NameTag output*:
        - NameTag 1: detailed output,
        - NameTag 2: XML.
4. transformation from *NameTag output*, to the *application's common format*, which is:
    - list of dictionaries (~array of JSONs),
    - each item is either
        - named entity in the form of
            - single token,
            - container (note: may contain both: named entities and non-named entities).
        - non-named entity.
    - each named or non-named token also contain:
        - lemma,
        - tag,
        - string of whitespaces that are before the token (`pre_whitespaces`)
5. extending *application's common format* with recommended replacements in order to pseudonymize the text,
6. return the result.


## Details

### Tagging
In all use cases we need to use MorphoDiTa's tokenizer, because of the need to be able to convert between (grammatical) cases (i.e. 1st case, 2nd case, ..., 7th case). That implies that NER recognizer has to receive tokens from MorphoDiTa.

### Recognizing (Named Entity Recognition)
NameTag 1 `recognize` method output is very detailed - i.e. it contains starting & ending positions of the individual tokens while iterating them, ...

On the other hand, NameTag 2 does not provide much of the information. Also, when sending tokens (in vertical format) from MorphoDiTa's tokenizer to the NameTag 2 recognizer it yields a problem that it handles whitespaces incorrectly (spaces before period (.), parentheses, ...).

Proposed solution is to tokenize input text using MorphoDiTa's tokenizer, then sending it to the NameTag 2 recognizer in vertical format (note that: `tokenizer_token_count == recognizer_token_count`), then the whitespaces will be reconstructed using `token_counter` (i.e. variable that maintains `token_index` while iterating over the text XML) through the detailed object from `morphoditawrapper.tokenize` where
```python
morphoditawrapper.tokenize(text)[token_counter] === {
    'start': token.start,
    'end': token.start + token.length,
    'token': text[token.start: token.start + token.length]
}
```

### Personal name pseudonymization
There are three modes of pseudonymization:

1. static,
2. random,
3. context.

*Static* is always taking the first option.

*Random* is taking random option.

*Context* has two cases. Personal name (first name or second name) is

- registered => take it as it was registered before,
- not yet registered => take next non-occupied name, and if there is no non-occupied name, start from beginning.

### Other pseudonymization
Pseudonymization of other named entities implements only *Static* and *Random* modes.

### Black mode
*Black mode* makes pseudonymized words black on black.

### Preserving (grammatical) cases
When the named entity to pseudonymize is found it is replaced with another word from the same category. I.e., when the first name 'Karel' is encountered it will be replaced with another first name, e.g. 'Jan'. Also, if it is encountered in the second case, i.e. 'Karla', it will be replaced with corresponding 'Jana'.

When the encountered named entity is composed of more words, e.g. 'Ústí nad Labem', it will be always pseudonymized with one-word named entity, e.g. 'Praha'. That decision was made for the simplicity. Other words will get discarded. From the 'Ústí nad Labem' we need to extract the case correctly. That is actually done by the first word, in the example 'Ústí'.

## Pseudonymization policies definitions

### Personal names

#### Notes
First name (`pf`) and second name (`ps`) might be container. Even though, I haven't found an example where `pf` is a container, but let's handle it same as `ps` container... Serialization process flattens these nested containers. Common policy is to pseudonymize the first encountered named entity of given type and discard others. Consider `John von Neumann`, where `von Neumann` is the last name container. It will be serialized as `container: [{'von': 'ps'}, {'Neumann': 'ps'}]`.

#### Process

##### First names
Take the first one and replace with another first name. Gender must be preserved. In case of Czech language, set the correct grammatical case.

Problem 1: MorphoDiTa does not know the case (we can't generate proper replacement form). Let's default to first case.

Problem 2: MorphoDiTa does not know gender. Default to masculinum (gender is needed in order to pick proper replacement word).

##### Second names
Same as first name.

Same problems as with first names and also:

Problem: Some names are in adjective form, e.g. `Franta Černý`, where `Černý` is adjective. We have to handle this situation properly.

If MorphoDiTa can't generate proper form of replacement, app will replace it with lemma of the replacement.

##### Middle names (`pm`)
Current policy is to remove the word.


# Documentation
To generate documentation, go to the `app/api/doc-util/` folder. Then execute
```bash
./generate-doc.sh ../README.md
```

You have to install [`pandoc`](https://pandoc.org/installing.html), [`mermaid-cli`](https://github.com/mermaid-js/mermaid-cli), and [`pandoc-mermaid`](https://github.com/timofurrer/pandoc-mermaid-filter).

Make sure you install `pandoc-mermaid` **locally**.
