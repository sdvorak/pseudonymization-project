"""
Entity recognition abstraction
"""

from abc import ABC, abstractmethod
from typing import List
import logging


class EntityRecognition(ABC):
    """
    Entity recognition abstraction
    """

    """
    Main methods.
    """

    def __init__(self):
        self.log = logging.getLogger('gunicorn.error')

    @abstractmethod
    def serialize(self, text: str, lang: str) -> List[dict]:
        """
        Perform named entity recognition on plain text. Implementation differs
        within NameTag 1 & NameTag 2 because NameTag 1 has its own structure whereas
        NameTag 2 returns XML.
        :param text: text to perform named entity recognition on,
        :param lang: language of the text 'text'.
        :return: list of dictionaries
        """

    """
    Static methods for output format.
    """
    @staticmethod
    def deserialize(ner_obj: List[dict]) -> str:
        """
        Deserialize NER object into the plain text.
        :param ner_obj: list of dictionaries
        :return: plain text described by 'ner_obj'
        """
        result_str = ''
        for el in ner_obj:
            if 'container_type' in el:
                for iel in el['entities']:
                    if 'recommended_replace' in iel and iel['recommended_replace'] == '':
                        continue

                    result_str += iel['pre_whitespaces'] + (
                        iel['recommended_replace'] if 'recommended_replace' in iel else iel['form'])
            else:
                if 'recommended_replace' in el and el['recommended_replace'] == '':
                    continue

                result_str += el['pre_whitespaces'] + (
                    el['recommended_replace'] if 'recommended_replace' in el else el['form'])

        return result_str

    @staticmethod
    def is_container(item) -> bool:
        """
        Check if NER item is container of entities.
        :param item: item
        :return: True iff 'item' is container of simple entities
        """
        return 'entities' in item

    @staticmethod
    def is_simple_entity(item) -> bool:
        """
        Check if NER item is simple named entity
        :param item: item
        :return: True iff 'item' is simple entity
        """
        return 'entity_type' in item

    @staticmethod
    def is_named_entity(item_or_container) -> bool:
        """
        Named entity is either simple NER item OR container of simple NER items.
        :param item_or_container: item or container
        :return: True iff 'item_or_container' is NER entity
        """
        return EntityRecognition.is_simple_entity(item_or_container) or\
               EntityRecognition.is_container(item_or_container)

    @staticmethod
    def get_nth_item(absolute_index: int, ner_repr):
        """
        In 'ner_repr' there are simple words (NOT named entities), simple named entities
        or container of entities. That implies that the index to the 'ner_repr' is not
        the i-th token! That is because containers contains more than one token.

        So what we need is a function that supports direct indexing by token index.
        :param absolute_index: index of token to be retrieved
        :param ner_repr: representation of NER process.
        :return: i-th token (in NER representation).
        """
        if absolute_index >= EntityRecognition.get_ner_repr_length(ner_repr):
            raise Exception('Out of bounds error')

        cur_item_idx = 0

        for item in ner_repr:
            if EntityRecognition.is_container(item):
                for c_item in item['entities']:
                    if cur_item_idx == absolute_index:
                        return c_item
                    cur_item_idx += 1
            else:
                if cur_item_idx == absolute_index:
                    return item
                cur_item_idx += 1

        # not possible to reach this statement
        assert False

    @staticmethod
    def get_ner_repr_length(ner_repr):
        """
        Get token count.
        :param ner_repr:
        :return:
        """
        cur_item_idx = 0

        for item in ner_repr:
            if EntityRecognition.is_container(item):
                cur_item_idx += len(item['entities'])
            else:
                cur_item_idx += 1

        return cur_item_idx
