"""
Tag Utility
"""

class TagWrapper:
    """
    Helper wrapper for the string representing the 'tag'. It provides semantic methods.
    """
    def __init__(self, tag):
        """
        Provide a tag.
        :param tag: tag
        """
        self.__tag = tag

    def get_tag(self):
        """
        cz: tag
        :return: tag
        """
        return self.__tag

    def part_of_speach(self):
        """
        cz: Slovní druh
        :return:
        """
        return self.__tag[0]

    def sub_pos(self):
        """
        cz: Slovní druh "detailněji"
        :return:
        """
        return self.__tag[1]

    def gender(self):
        """
        cz: Rod
        :return:
        """
        return self.__tag[2]

    def number(self):
        """
        cz: Číslo
        :return:
        """
        return self.__tag[3]

    def case(self):
        """
        cz: Pád
        :return:
        """
        return self.__tag[4]

    def possesors_gender(self):
        """
        cz: Rod toho, komu přivlastnujeme???
        :return:
        """
        return self.__tag[5]

    def degree_of_comparison(self):
        """
        cz: For adjectives
        :return:
        """
        return self.__tag[9]

    def negation(self):
        """
        cz: Negace (A=Affirmative/N=Negative)
        :return:
        """
        return self.__tag[10]

    def variant(self):
        """
        :return:
        """
        return self.__tag[14]

    def __change_char_helper(self, idx, change_to):
        """
        Changes char of a string to the specified value.
        :param idx: index to be changed
        :param change_to: new character
        :return: new TagWrapper with the new string
        """
        new_tag = list(self.__tag)
        new_tag[idx] = change_to
        return TagWrapper(''.join(new_tag))

    def change_part_of_speach(self, change_to):
        """
        Changes part of the speach (slovní druh).
        :param change_to:
        :return:
        """
        return self.__change_char_helper(0, change_to)

    def change_sub_pos(self, change_to):
        """
        Changes sub pos
        :param change_to:
        :return:
        """
        return self.__change_char_helper(1, change_to)

    def change_gender(self, change_to):
        """
        Changes gender (M/F/N).
        :param change_to:
        :return:
        """
        return self.__change_char_helper(2, change_to)

    def change_number(self, change_to):
        """
        Changes number (S=Singular/P=Plural)
        :param change_to:
        :return:
        """
        return self.__change_char_helper(3, change_to)

    def change_case(self, change_to):
        """
        Change case (1, 2, ..., 7)
        :param change_to:
        :return:
        """
        return self.__change_char_helper(4, change_to)

    def change_possesors_gender(self, change_to):
        """
        Change possesors gender.
        :param change_to:
        :return:
        """
        return self.__change_char_helper(5, change_to)

    def change_degree_of_comparison(self, change_to):
        """
        cz: For adjectives
        :return:
        """
        return self.__change_char_helper(9, change_to)

    def change_negation(self, change_to):
        """
        cz: Negace (A=Affirmative/N=Negative)
        :return:
        """
        return self.__change_char_helper(10, change_to)

    def change_variant(self, change_to):
        """
        cz: Negace (A=Affirmative/N=Negative)
        :return:
        """
        return self.__change_char_helper(14, change_to)

    @staticmethod
    def gender_matching_options(filtering_gender, options):
        """
        There are multiple gender marks for 'general' gender. General gender is
        masculine 'M', feminine 'F', and neutrum 'N'. But there are also specific
        genders for masculine inanimate, etc.
        TODO: make better logic for masculine/feminine/neutrum mixture marks.
        :param filtering_gender: gender (char)
        :param options: all pseudonymization options
        :return: pseudonymization options matching 'filtering_gender' in more
                 general meaning. If the 'filtering_gender' is not matching one of
                 'M', 'I', 'Y', 'F', 'N', then it is considered neutrum.
        """
        masculine_genders = ['M', 'I', 'Y']
        feminine_genders = ['F']
        neutrum_genders = ['N']

        relative_gender = neutrum_genders
        if filtering_gender in masculine_genders:
            relative_gender = masculine_genders
        elif filtering_gender in feminine_genders:
            relative_gender = feminine_genders

        return list(
            filter(
                lambda option: TagWrapper(option['tag_template']).gender() in relative_gender,
                options
            )
        )

    @staticmethod
    def get_replacement_gender(tag):
        """
        Return gender of which we need to replace current word.
        If it is noun, then the replacement word should be its gender,
        on the other hand, when it is adjective, then it should be
        replaced with word of gender equal to possesors gender.
        :param tag: tag of the word to be replaced.
        :return: gender of replacement word
        """
        # if noun => take its gender
        # if adjective => take possesive gender
        if (tag.part_of_speach() == 'N' and tag.gender() == 'M') or \
           (tag.part_of_speach() == 'A' and tag.possesors_gender() == 'M'):
            return 'M'
        elif (tag.part_of_speach() == 'N' and tag.gender() == 'F') or \
             (tag.part_of_speach() == 'A' and tag.possesors_gender() == 'F'):
            return 'F'

        # defaulting
        return 'M'
