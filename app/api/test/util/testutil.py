"""
Class to be extend by other test classes.

Following AAA (arrange, act, assert) pattern.
"""
import unittest
from typing import List, Union

from entityrecognition import EntityRecognition
from ppolicy.policy import Policy
from pseudonymizationpolicystructures import PersonalNameReplacingMode, OtherReplacingMode
from nametag1wrapper import NameTag1Wrapper
from nametag2wrapper import NameTag2Wrapper


class TestUtil(unittest.TestCase):
    """
    Test util
    """
    @classmethod
    def setUpClass(cls):
        """

        :return:
        """
        cls.maxDiff = None

    def perform_pseudonymization_tests(
            self,
            nt_wrapper: Union[
                NameTag1Wrapper,
                NameTag2Wrapper
            ],
            test_cases: List[dict]
    ) -> None:
        """
        Perform masking tests given by 'test_cases' & NameTag wrapper 'nt'
        :param nt_wrapper: NameTag wrapper (either 'mg.NT1', or 'mg.NT2')
        :param test_cases: list of test cases. Each item has the format
                           {'input': $input_txt, 'expected': $expected_txt}
        :return: None
        """
        for test_case in test_cases:
            # act
            inp = test_case['input']
            expected = test_case['expected']

            items = Policy.pseudonymization(
                inp,
                Policy.ALL,
                'cs',
                PersonalNameReplacingMode.STATIC,
                OtherReplacingMode.STATIC,
                nt_wrapper
            )

            self.assertMultiLineEqual(EntityRecognition.deserialize(items), expected)

    def perform_serialization_and_deserialization(
            self,
            nt_wrapper: Union[
                NameTag1Wrapper,
                NameTag2Wrapper
            ],
            test_cases: List[dict]
    ) -> None:
        """
        Perform serialization & deserialization tests given by 'test_cases' & NameTag wrapper 'nt'
        :param nt_wrapper: NameTag wrapper (either 'mg.NT1', or 'mg.NT2')
        :param test_cases: list of test cases. Each item has the format
                           {'input': $input_txt, 'expected': $expected_txt}
        :return: None
        """
        for test_case in test_cases:
            # act
            inp = test_case['input']
            expected = test_case['expected']

            serialized = nt_wrapper.serialize(inp)

            self.assertMultiLineEqual(EntityRecognition.deserialize(serialized), expected)
