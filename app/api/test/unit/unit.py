"""
Unit tests.

Following AAA (arrange, act, assert) pattern.
"""
import unittest

import mg


class UnitTest(unittest.TestCase):
    """
    Unit tests
    """
    @classmethod
    def setUpClass(cls):
        """

        :return:
        """
        mg.init()

    def test_morphodita_tokenize(self) -> None:
        """
        Test MorphoDiTa tokenize method.
        :return:
        """
        # arrange
        text = 'Vědec John von Neumann je známý   svým přínosem informatice. Vědec von Neumann žil        v    19. století.'

        # act
        tokens = mg.MW.flatten_tokenize(text)
        just_tokens = list(map(lambda token: token['token'], tokens))

        # assert
        self.assertEqual(
            ['Vědec', 'John', 'von', 'Neumann', 'je', 'známý', 'svým',
             'přínosem', 'informatice', '.', 'Vědec', 'von',
             'Neumann', 'žil', 'v', '19', '.', 'století', '.'], just_tokens)


if __name__ == '__main__':
    unittest.main()
