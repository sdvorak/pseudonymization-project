"""
End to End tests.

Following AAA (arrange, act, assert) pattern.
"""
import unittest

from test.util.testutil import TestUtil
import mg


class E2ETest(TestUtil):
    """
    End to End tests
    """
    @classmethod
    def setUpClass(cls):
        """

        :return:
        """
        mg.init()

    ##############################
    # Serialization tests follows:
    ##############################
    def test_ner_object_can_be_reverted_edge(self) -> None:
        # arrange
        test_cases = [
            {
                'input': '',
                'expected': ''
            },
            {
                'input': 'x',
                'expected': 'x'
            },
            {
                'input': 'x y',
                'expected': 'x y'
            },
        ]

        # act & assert
        self.perform_serialization_and_deserialization(mg.NT1, test_cases)
        self.perform_serialization_and_deserialization(mg.NT2, test_cases)

    def test_ner_object_can_be_reverted_basic(self) -> None:
        # arrange
        test_cases = [
            {
                'input': 'Vědec John von Neumann je známý        svým přínosem informatice. Vědec    von Neumann žil v 19. století.',
                'expected': 'Vědec John von Neumann je známý        svým přínosem informatice. Vědec    von Neumann žil v 19. století.'
            },
            {
                'input': 'Antonín Dvořák složil Vltavu v 19. století. Je to Karlova oblíbená skladba. V neděli jí hraje Český národní symfonický orchestr v Pražském Rudolfinu.',
                'expected': 'Antonín Dvořák složil Vltavu v 19. století. Je to Karlova oblíbená skladba. V neděli jí hraje Český národní symfonický orchestr v Pražském Rudolfinu.'
            },
            {
                'input': 'Tomáš Garrigue Masaryk byl první prezident    Československé  republiky, mj. Zasloužil se o vznik Československé republiky. V  roce 1896 odešel do Vídně.',
                'expected': 'Tomáš Garrigue Masaryk byl první prezident    Československé  republiky, mj. Zasloužil se o vznik Československé republiky. V  roce 1896 odešel do Vídně.'
            },
        ]

        # act & assert
        self.perform_serialization_and_deserialization(mg.NT1, test_cases)

    def test_ner_object_can_be_reverted_lorem_ipsum(self) -> None:
        f_input = open('../testfiles/lorem-5-paragraphs.txt')
        content = f_input.read()

        # 'input' === 'expected'
        test_cases = [{
            'input': content,
            'expected': content
            }
        ]

        self.perform_serialization_and_deserialization(mg.NT1, test_cases)
        self.perform_serialization_and_deserialization(mg.NT2, test_cases)

        f_input.close()

    #################################
    # Pseudonymization tests follows:
    #################################
    def test_edge_cases(self) -> None:
        # arrange
        test_cases = [
            {
                'input': '',
                'expected': ''
            },
            {
                'input': 'x',
                'expected': 'x'
            },
            {
                'input': 'x y',
                'expected': 'x y'
            },
        ]

        # act & assert
        self.perform_pseudonymization_tests(mg.NT1, test_cases)

    def test_happy_day_scenarios_on_names(self) -> None:
        # arrange
        test_cases = [
            {
                'input': 'Před hodinou odešel ke Karlovi.',
                'expected': 'Před hodinou odešel k Janovi.'
            },
            {
                'input': 'Maxmilián je můj kamarád.',
                'expected': 'Jan je můj kamarád.'
            },
            {
                'input': 'Poslouchali jsme Smetanovu Vltavu.',
                'expected': 'Poslouchali jsme Dvořákovu Ohři.'
            },
            {
                'input': 'Tomáš Garrigue Masaryk byl první prezident Československé republiky, mj.',
                'expected': 'Jan Dvořák byl první prezident Francie, mj.'
            },
            {
                # 'input': 'Jak se jmenuje? Novotný Franta.',   # nepozná, že 'Novotný' je příjmení
                # 'expected': 'Jak se jmenuje? Novák Jan.'
                'input': 'Jak se jmenuje? Dvořák Franta.',
                'expected': 'Jak se jmenuje? Dvořák Jan.'
            },
            {
                #                                        |||
                #                     NOTE the 3 spaces: vvv
                'input': 'Vědec John von Neumann je známý   svým přínosem informatice. Vědec von Neumann žil        v    19. století.',
                'expected': 'Vědec Jan Dvořák je známý   svým přínosem informatice. Vědec Dvořák žil        v    30. století.'
            }
        ]

        # act & assert
        self.perform_pseudonymization_tests(mg.NT1, test_cases)

    def test_indents_preserve(self) -> None:
        test_cases = [
            {
                'input':
"""Karel je mužské jméno. Karel Čapek byl tedy muž. Čapek napsal Válku s mloky. Karel Čapek napsal Bílou nemoc.
            
 Karel
  Karla
   Karlovi
    Karla
     Karle!
      Karlovi
       Karlem""",
                'expected':
"""Jan je mužské jméno. Jan Dvořák byl tedy muž. Dvořák napsal Válku s mloky. Jan Dvořák napsal Bílou nemoc.
            
 Jan
  Dvořáka
    Jana
     Dvořáku!
      Liberci"""
            }
        ]
        self.perform_pseudonymization_tests(mg.NT1, test_cases)

    def test_lorem_ipsum_paragraphs(self):
        f_input = open('../testfiles/lorem-5-paragraphs.txt')
        f_expected = open('../testfiles/lorem-5-paragraphs-expected.txt')

        test_cases = [{
            'input': f_input.read(),
            'expected': f_expected.read()
            }
        ]

        self.perform_pseudonymization_tests(mg.NT1, test_cases)

        f_input.close()
        f_expected.close()


if __name__ == '__main__':
    unittest.main()
