# Testing

Testing is divided into following sections

- unit tests,
- end to end tests.

## What is currently tested?

### Serialization
Serialization is tested by possibility of deserialization.

1. edge cases (empty string, ...),
2. single sentence,
3. multiple sentences,
4. paragraphs (lorem ipsum).

### Pseudonymization
Tests can be performed just on NameTag 1, because same pseudonymization algorithm applies for NameTag 2.

1. edge cases (empty string, ...),
2. single sentence,
3. multiple sentences,
4. paragraphs (lorem ipsum).
