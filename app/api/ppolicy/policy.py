from pseudonymizationpolicystructures import PersonalNameReplacingMode, OtherReplacingMode

from ppolicy.cspolicy import PolicyFacade as CsPolicyFacade
from ppolicy.enpolicy import PolicyFacade as EnPolicyFacade


class Policy:
    ALL = [
        'P',
        'pf', 'ps', 'pm',
        'gu', 'gs', 'gh', 'gu', 'gc',
        'ty',
        'na', 'no',
        'if', 'ic',
        'o', 'o_', 'oa', 'op', 'or',
        'mn'
    ]

    @staticmethod
    def pseudonymization(text: str, conf: list, lang: str,
                         personal_name_replacing_mode: PersonalNameReplacingMode,
                         other_replacing_mode: OtherReplacingMode,
                         entityrecognition):
        items = entityrecognition.serialize(text, lang)

        pf = []

        if lang == 'cs':
            pf = CsPolicyFacade(
                items,
                conf,
                personal_name_replacing_mode,
                other_replacing_mode
            )
        elif lang == 'en':
            pf = EnPolicyFacade(
                items,
                conf,
                personal_name_replacing_mode,
                other_replacing_mode
            )

        # iterate over items (i.e. simple words, simple named entities, or container) &
        # apply appropriate policy, if it is simple named entity or container & user
        # wants to pseudonymize it.
        # PolicyFacade works on 'items' variable by reference.
        for idx in range(len(items)):
            pf.mask(items[idx], idx)

        return items
