"""
English pseudonymization policies.
"""

import re

import random

from util.tagutil import TagWrapper

from ppolicy.commonpolicy import GeneralPolicyFacade, NERMaskingPolicy
from pseudonymizationpolicystructures import PersonalNameReplacingMode, OtherReplacingMode


from entityrecognition import EntityRecognition

import logging
log = logging.getLogger('gunicorn.error')


class PolicyFacade(GeneralPolicyFacade):
    """
    Decision facade for selecting appropriate policy.
    """

    def __init__(
            self,
            context: dict,
            selected_entities: list,
            personal_name_replacing_mode: PersonalNameReplacingMode
                = PersonalNameReplacingMode.STATIC,
            other_replacing_mode: OtherReplacingMode
                = OtherReplacingMode.STATIC
    ):
        """
        :param context:
        :param selected_entities:
        :param personal_name_replacing_mode:
        :param other_replacing_mode:
        """
        self.selected_entities = [
            'PER' if any(x in selected_entities for x in ['P', 'ps', 'pf']) else None,
            'ORG' if any(x in selected_entities for x in ['if', 'ic', 'o_', 'oa', 'op', 'or']) else None,
            'LOC' if any(x in selected_entities for x in ['gu', 'gc', 'gs', 'gh']) else None,
            'MISC'
        ]

        super().__init__(
            {
                'PER': ENPolicy.NamePolicy,
                'ORG': ENPolicy.CompanyOrganizationPolicy,
                'LOC': ENPolicy.TownPolicy,
                'MISC': ENPolicy.PeriodicalPolicy
            },
            context,
            self.selected_entities,
            personal_name_replacing_mode,
            other_replacing_mode
        )

        self.context = context
        self.personal_name_replacing_mode = personal_name_replacing_mode
        self.other_replacing_mode = other_replacing_mode

        ENPolicy.destruct()


class ENPolicy:
    @staticmethod
    def destruct():
        """
        Context mode needs to restart mapping
        of replacements each request.
        :return: None
        """
        ENPolicy.NamePolicy.m = {}

        ENPolicy.NamePolicy.m_pf_count = 0
        ENPolicy.NamePolicy.f_pf_count = 0

        ENPolicy.NamePolicy.m_ps_count = 0
        ENPolicy.NamePolicy.f_ps_count = 0

    class BasicPolicy(NERMaskingPolicy):
        """
        Basic Policy that preserve 'case' & handles prepositions.
        """

        def __init__(self, context, conf, ps_options):
            """
            :param context:
            :param conf:
            :param ps_options: list of pairs [(word, tag_template: TagWrapper)]
            """
            super().__init__(context, conf)
            self.ps_options = ps_options

        def mask(self, item, start_idx,
                 replacing_mode: PersonalNameReplacingMode = PersonalNameReplacingMode.STATIC,
                 other_replacing_mode: OtherReplacingMode = OtherReplacingMode.STATIC):
            # select replacement
            replacement = None

            relevant_pseudonymization_options = self.ps_options

            if other_replacing_mode == OtherReplacingMode.RANDOM:
                # randint has inclusive bounds
                idx = random.randint(0, len(relevant_pseudonymization_options) - 1)
                replacement = relevant_pseudonymization_options[idx]
            elif other_replacing_mode == OtherReplacingMode.STATIC:
                replacement = relevant_pseudonymization_options[0]

            replacement_word = replacement['form']

            if 'after_function' in replacement:
                replacement_word = replacement['after_function'](replacement_word)

            item['recommended_replace'] = replacement_word

        def mask_container(
                self,
                container,
                start_idx,
                replacing_mode: PersonalNameReplacingMode
                    = PersonalNameReplacingMode.STATIC,
                other_replacing_mode: OtherReplacingMode
                    = OtherReplacingMode.STATIC
        ):
            was_replaced = False
            for ne in container['entities']:
                if not was_replaced:
                    self.mask(ne, start_idx, replacing_mode, other_replacing_mode)
                    was_replaced = True
                else:
                    ne['recommended_replace'] = ''

    """
    Basic policies.
    """

    class TownPolicy(BasicPolicy):
        """
        Town name masking policy.
        """

        def __init__(self, context, conf):
            super().__init__(context, conf, [
                        {
                            'form': 'Prague'
                        },
                        {
                            'form': 'Paris'
                        },
                        {
                            'form': 'Munich'
                        },
                        {
                            'form': 'Ottawa'
                        }
                    ])

    class CompanyOrganizationPolicy(BasicPolicy):
        """
        Company, organization, etc. masking policy.
        """

        def __init__(self, context, conf):
            super().__init__(context, conf, [
                {
                    'form': 'Rising',
                    'after_function': lambda s: s.capitalize()
                },
                {
                    'form': 'Good',
                    'after_function': lambda s: s.capitalize()
                },
                {
                    'form': 'Flexible',
                    'after_function': lambda s: s.capitalize()
                }
            ])

    class PeriodicalPolicy(BasicPolicy):
        """
        'mn' - periodical
        """

        def __init__(self, context, conf):
            super().__init__(context, conf, [
                {
                    'form': 'News',
                    'after_function': lambda s: s.capitalize()
                },
                {
                    'form': 'Journal',
                    'after_function': lambda s: s.capitalize()
                },
                {
                    'form': 'Info',
                    'after_function': lambda s: s.capitalize()
                }
            ])

    """
    Non-basic policies.
    """

    class NamePolicy(NERMaskingPolicy):
        """
        Personal name masking policy.
        """
        # identity mapping
        # i.e. key is [lemma(what_was_encountered)...]
        # value is index to the corresponding array with identity
        m = {}
        m_pf_count = 0
        f_pf_count = 0

        m_ps_count = 0
        f_ps_count = 0

        def __init__(self, context, conf):
            """
            :param context:
            :param conf:
            """
            super().__init__(context, conf)

            # sometimes surname is adjective, e.g. Bohuslav Černý, where Černý is adjective whereas
            # Pavel Dvořák, where Dvořák is a noun.
            self.ps_options = {
                # M = Masculinum
                'M': {
                    'PER': [
                        {
                            'N': 'James',
                        },
                        {
                            'N': 'Robert'
                        },
                        {
                            'N': 'John',
                        },
                        {
                            'N': 'Michael',
                        },
                        {
                            'N': 'David',
                        }
                    ],
                },
                # F = Femininum
                'F': {
                    'PER': [
                        {
                            'N': 'Mary',
                        },
                        {
                            'N': 'Patricia',
                        },
                        {
                            'N': 'Jennifer',
                        },
                        {
                            'N': 'Linda',
                        },
                    ]
                }
            }

        def mask(self,
                 item,
                 start_idx,
                 replacing_mode: PersonalNameReplacingMode = PersonalNameReplacingMode.STATIC,
                 other_replacing_mode: OtherReplacingMode = OtherReplacingMode.STATIC):
            """
            How to mask the first name of surname.
            :param other_replacing_mode:
            :param item:
            :param start_idx:
            :param replacing_mode:
            :param context_mode:
            :return:
            """
            def get_replacement_index():
                """
                Get index to the named entity replacement if it was not registered.
                :return:
                """
                nonlocal item
                entity_type = item['entity_type']

                MASCULINE_NAME_COUNT = len(self.ps_options['M'][entity_type])
                FEMININE_NAME_COUNT = len(self.ps_options['F'][entity_type])

                index = 0
                if replacing_mode == PersonalNameReplacingMode.STATIC:
                    # static mode => index is always zero
                    index = 0
                elif replacing_mode == PersonalNameReplacingMode.RANDOM:
                    # random mode => index is random with bounds given by gender
                    if TagWrapper.get_replacement_gender(tag) == 'M':
                        index = random.randint(0, MASCULINE_NAME_COUNT - 1)
                    if TagWrapper.get_replacement_gender(tag) == 'F':
                        index = random.randint(0, FEMININE_NAME_COUNT - 1)
                elif replacing_mode == PersonalNameReplacingMode.CONTEXT:
                    # context mode => consider we did not register this name
                    if TagWrapper.get_replacement_gender(tag) == 'M':
                        index = ENPolicy.NamePolicy.m_pf_count % MASCULINE_NAME_COUNT
                    if TagWrapper.get_replacement_gender(tag) == 'F':
                        index = ENPolicy.NamePolicy.f_pf_count % FEMININE_NAME_COUNT

                return index

            def context_registration(tag):
                nonlocal item
                lemma = item['lemma']
                entity_type = item['entity_type']

                index = get_replacement_index()

                if TagWrapper.get_replacement_gender(tag) == 'M':
                    identity = self.ps_options['M'][entity_type][index]
                    ENPolicy.NamePolicy.m[lemma.capitalize()] = identity
                    ENPolicy.NamePolicy.m_pf_count += 1
                elif TagWrapper.get_replacement_gender(tag) == 'F':
                    identity = self.ps_options['F'][entity_type][index]
                    ENPolicy.NamePolicy.m[lemma.capitalize()] = identity
                    ENPolicy.NamePolicy.f_pf_count += 1

            lemma = item['lemma']
            entity_type = item['entity_type']
            tag = TagWrapper(item['tag'])

            # it is entity that we do not want to pseudonymize
            if entity_type not in self.conf:
                return

            # bad part of speach (i.e. not noun nor adjective)
            if tag.part_of_speach() not in ['N', 'A']:
                return

            #####################
            # Make a registration
            #####################

            # gender for selecting appropriate replacement word
            replacing_gender = TagWrapper.get_replacement_gender(tag)

            # not a context mode
            if replacing_mode != PersonalNameReplacingMode.CONTEXT:
                ENPolicy.NamePolicy.m[lemma.capitalize()] =\
                    self.ps_options[replacing_gender][entity_type][get_replacement_index()]
            # not registered and we are in context mode ==> we should register
            elif lemma.capitalize() not in ENPolicy.NamePolicy.m:
                context_registration(tag)

            ####################
            # Create replacement
            ####################

            # do not consider adjectives
            to_replace = ENPolicy.NamePolicy.m[lemma.capitalize()]['N']
            item['recommended_replace'] = to_replace.capitalize()

        def mask_container(
                self,
                container,
                start_idx,
                replacing_mode: PersonalNameReplacingMode =
                    PersonalNameReplacingMode.STATIC,
                other_replacing_mode: OtherReplacingMode =
                    OtherReplacingMode.STATIC
        ):
            """
            How to mask container with first name and second name.
            Container has field 'entities', which is array of entities.
            So to know if the object is container, we just find out if it has field 'entities'.
            :param other_replacing_mode:
            :param replacing_mode:
            :param container: named entity container
            :return: masked named entity container
            """
            firstword_encountered = False

            ne_iter = iter(range(len(container['entities'])))
            for ne_idx in ne_iter:
                # entity inside 'P' or 'ps' container
                ne = container['entities'][ne_idx]

                if 'entity_type' in ne and ne['entity_type'] not in self.conf:
                    start_idx += 1
                    continue

                if not firstword_encountered:
                    ne['entity_type'] = 'PER'
                    self.mask(ne, start_idx, replacing_mode, other_replacing_mode)
                    firstword_encountered = True
                else:
                    ne['recommended_replace'] = ''

                start_idx += 1
