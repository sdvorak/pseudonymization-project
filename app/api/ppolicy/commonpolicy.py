"""
Policy facade - makes mapping between
named entity types and their implementations
with respect to language.
"""
import random
from abc import abstractmethod, ABC

from entityrecognition import EntityRecognition
from pseudonymizationpolicystructures import PersonalNameReplacingMode, OtherReplacingMode


class NERMaskingPolicy(ABC):
    """
    Abstract class that defines interface for masking specific
    named entity (e.g. first names, surname, ...).
    """
    def __init__(self, context, conf):
        """
        :param context:
        :param conf: list containing named entity types to be masked
        """
        self.context = context
        self.ps_options = {}
        self.conf = conf

    @abstractmethod
    def mask(
            self,
            item,
            start_idx,
            replacing_mode: PersonalNameReplacingMode = PersonalNameReplacingMode.STATIC,
            other_replacing_mode: OtherReplacingMode = OtherReplacingMode.STATIC
    ):
        """
        How to mask found word.
        :param other_replacing_mode:
        :param item:
        :param start_idx:
        :param replacing_mode:
        :return:
        """

    @abstractmethod
    def mask_container(
            self,
            container,
            start_idx,
            replacing_mode: PersonalNameReplacingMode = PersonalNameReplacingMode.STATIC,
            other_replacing_mode: OtherReplacingMode = OtherReplacingMode.STATIC
    ):
        """
        How to mask found container.
        :param other_replacing_mode:
        :param container:
        :param start_idx:
        :param replacing_mode:
        :return:
        """


class GeneralPolicyFacade(ABC):
    def __init__(
            self,
            ENTITY_TYPE_MAPPING,
            context: dict,
            selected_entities: list,
            personal_name_replacing_mode: PersonalNameReplacingMode
                = PersonalNameReplacingMode.STATIC,
            other_replacing_mode: OtherReplacingMode
                = OtherReplacingMode.STATIC,
    ):
        self.context = context
        self.selected_entities = selected_entities
        self.personal_name_replacing_mode = personal_name_replacing_mode
        self.other_replacing_mode = other_replacing_mode
        self.ENTITY_TYPE_MAPPING = ENTITY_TYPE_MAPPING

    def mask(self, item, start_idx):
        """
        :param item:
        :param start_idx:
        :return:
        """
        if not EntityRecognition.is_named_entity(item):
            return

        if EntityRecognition.is_container(item):
            container_type = item['container_type']

            if container_type not in self.selected_entities:
                return

            self.ENTITY_TYPE_MAPPING[container_type](self.context, self.selected_entities)\
                .mask_container(
                    item,
                    start_idx,
                    self.personal_name_replacing_mode,
                    self.other_replacing_mode
                )

        else:
            entity_type = item['entity_type']

            if entity_type not in self.selected_entities:
                return

            # middle name
            if entity_type == 'pm':
                item['recommended_replace'] = ''
                return

            self.ENTITY_TYPE_MAPPING[entity_type](self.context, self.selected_entities)\
                .mask(
                    item,
                    start_idx,
                    self.personal_name_replacing_mode,
                    self.other_replacing_mode
                )

    class AgePolicy(NERMaskingPolicy):
        """
        Age masking policy.
        """
        def __init__(self, context, conf):
            super().__init__(context, conf)

        def mask(self, item, start_idx,
                 replacing_mode: PersonalNameReplacingMode = PersonalNameReplacingMode.STATIC,
                 other_replacing_mode: OtherReplacingMode = OtherReplacingMode.STATIC):
            if other_replacing_mode == OtherReplacingMode.STATIC:
                item['recommended_replace'] = '12'
            elif other_replacing_mode == OtherReplacingMode.RANDOM:
                item['recommended_replace'] = str(
                    int(item['form']) +
                    (random.randint(-10, 10)
                        if other_replacing_mode == OtherReplacingMode.RANDOM
                        else 9)
                )

        def mask_container(
                self,
                container,
                start_idx,
                replacing_mode: PersonalNameReplacingMode = PersonalNameReplacingMode.STATIC,
                other_replacing_mode: OtherReplacingMode = OtherReplacingMode.STATIC
        ):
            raise Exception('Age can\'t be container.')

    class OrdinalNumberPolicy(NERMaskingPolicy):
        """
        Ordinal number masking policy.
        """
        def __init__(self, context, conf):
            super().__init__(context, conf)

        def mask(
            self,
            item,
            start_idx,
            replacing_mode: PersonalNameReplacingMode =
                PersonalNameReplacingMode.STATIC,
            other_replacing_mode: OtherReplacingMode =
                OtherReplacingMode.STATIC
        ):
            if other_replacing_mode == OtherReplacingMode.STATIC:
                item['recommended_replace'] = '30'
            elif other_replacing_mode == OtherReplacingMode.RANDOM:
                # more universal is probably number >= 20, because in Czech it
                # can mean [ve 20. letech] = 've dvacátých letech'
                # n = round(int(item['form']))
                allowed_final = list(range(20, 100 - 10, 10))
                selected_idx = random.randint(0, len(allowed_final) - 1)
                item['recommended_replace'] = str(
                    round(allowed_final[selected_idx])
                        if other_replacing_mode == OtherReplacingMode.RANDOM else 10
                )

        def mask_container(self, container, start_idx,
                           replacing_mode: PersonalNameReplacingMode = PersonalNameReplacingMode.STATIC,
                           other_replacing_mode: OtherReplacingMode = OtherReplacingMode.STATIC):
            self.mask(container['entities'][0], start_idx, replacing_mode, other_replacing_mode)

    class YearsPolicy(NERMaskingPolicy):
        """
        Years masking policy.
        """
        def __init__(self, context, conf):
            super().__init__(context, conf)

        def mask(
            self,
            item,
            start_idx,
            replacing_mode: PersonalNameReplacingMode =
                PersonalNameReplacingMode.STATIC,
            other_replacing_mode: OtherReplacingMode =
                OtherReplacingMode.STATIC
        ):
            if other_replacing_mode == OtherReplacingMode.RANDOM:
                item['recommended_replace'] = str(int(item['form']) + random.randint(10, 100))
            elif other_replacing_mode == OtherReplacingMode.STATIC:
                item['recommended_replace'] = '1920'

        def mask_container(self, container, start_idx,
                           replacing_mode: PersonalNameReplacingMode = PersonalNameReplacingMode.STATIC,
                           other_replacing_mode: OtherReplacingMode = OtherReplacingMode.STATIC):
            raise Exception('Year can\'t be container.')
