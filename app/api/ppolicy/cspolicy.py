"""
Czech pseudonymization policies.
"""

from abc import ABC, abstractmethod
import re

import random

import mg
from util.tagutil import TagWrapper

from ppolicy.commonpolicy import NERMaskingPolicy, GeneralPolicyFacade
from pseudonymizationpolicystructures import PersonalNameReplacingMode, OtherReplacingMode


from entityrecognition import EntityRecognition

import logging
log = logging.getLogger('gunicorn.error')


class PolicyFacade(GeneralPolicyFacade):
    """
    Decision facade for selecting appropriate policy.
    """

    def __init__(
            self,
            context: dict,
            selected_entities: list,
            personal_name_replacing_mode: PersonalNameReplacingMode
                = PersonalNameReplacingMode.STATIC,
            other_replacing_mode: OtherReplacingMode
                = OtherReplacingMode.STATIC
    ):
        """
        :param context:
        :param selected_entities:
        :param personal_name_replacing_mode:
        :param other_replacing_mode:
        """
        super().__init__(
            {
                'P': CSPolicy.NamePolicy,
                'pf': CSPolicy.NamePolicy,
                'ps': CSPolicy.NamePolicy,  # note that surname can be container as well
                'T': CSPolicy.TimeExpression,
                'gu': CSPolicy.TownPolicy,  # first name
                'gc': CSPolicy.CountryPolicy,  # surname
                'gs': CSPolicy.StreetSquarePolicy,
                'gh': CSPolicy.HydronymPolicy,  # hydronyms
                'if': CSPolicy.CompanyOrganizationPolicy,  # 'if' - companies, concerns
                'ic': CSPolicy.CultEducSciPolicy,  # 'ic' - cult./educ./scient. institution,
                'o_': CSPolicy.ArtifactPolicy,  # underspecified
                'oa': CSPolicy.ArtifactPolicy,  # cultural artifacts (books, movies)
                'op': CSPolicy.ArtifactPolicy,  # products
                'or': CSPolicy.ArtifactPolicy,  # directives/norms
                'mn': CSPolicy.PeriodicalPolicy,
                'no': self.OrdinalNumberPolicy,
                'na': self.AgePolicy,
                'ty': self.YearsPolicy
            },
            context,
            selected_entities,
            personal_name_replacing_mode,
            other_replacing_mode
        )

        self.context = context
        self.selected_entities = selected_entities

        self.personal_name_replacing_mode = personal_name_replacing_mode
        self.other_replacing_mode = other_replacing_mode

        CSPolicy.destruct()


class CSPolicy:
    @staticmethod
    def destruct():
        """
        Context mode needs to restart mapping
        of replacements each request.
        :return: None
        """
        CSPolicy.NamePolicy.m = {}

        CSPolicy.NamePolicy.m_pf_count = 0
        CSPolicy.NamePolicy.f_pf_count = 0

        CSPolicy.NamePolicy.m_ps_count = 0
        CSPolicy.NamePolicy.f_ps_count = 0

    class BasicPolicy(NERMaskingPolicy):
        """
        Basic Policy that preserve 'case' & handles prepositions.
        """

        def __init__(self, context, conf, ps_options):
            """
            :param context:
            :param conf:
            :param ps_options: list of pairs [(word, tag_template: TagWrapper)]
            """
            super().__init__(context, conf)
            self.ps_options = ps_options

        def mask(
                self,
                item,
                start_idx,
                replacing_mode: PersonalNameReplacingMode =
                    PersonalNameReplacingMode.STATIC,
                other_replacing_mode: OtherReplacingMode =
                    OtherReplacingMode.STATIC
        ):
            # select replacement
            replacement = None

            # consider gender (in/animate)
            relevant_pseudonymization_options = TagWrapper.gender_matching_options(
                TagWrapper(item['tag']).gender(),
                self.ps_options
            )

            if other_replacing_mode == OtherReplacingMode.RANDOM:
                # randint has inclusive bounds
                idx = random.randint(0, len(relevant_pseudonymization_options) - 1)
                replacement = relevant_pseudonymization_options[idx]
            elif other_replacing_mode == OtherReplacingMode.STATIC:
                replacement = relevant_pseudonymization_options[0]

            # solve prepositions
            # TODO: english text does not need to solve Czech prepositions
            #       but they are distinct, so it might not be a problem...
            if 'prepositions' in replacement and 0 <= start_idx - 1 < len(self.context) and \
               'form' in self.context[start_idx - 1]:
                if self.context[start_idx - 1]['form'] in replacement['prepositions']:
                    self.context[start_idx - 1]['recommended_replace'] =\
                        replacement['prepositions'][self.context[start_idx - 1]['form']]

            case = TagWrapper(item['tag']).case()
            tag = TagWrapper(replacement['tag_template']).change_case(case)

            # if there is preposition 'v', 've' or 'na', then it is probably 6th case.
            # It might not be true in all the cases so let's constraint this heuristic
            # only to unknown cases & 4th case (which usually does not have this preposition).
            # TODO: more analysis.
            if 0 <= start_idx - 1 and 'form' in self.context[start_idx - 1]\
                    and self.context[start_idx - 1]['form'] in ['v', 've', 'na']\
                    and tag.case() in ['X', '4']:
                tag = tag.change_case('6')

            # generate correct form of replacement
            generated = mg.MW.generate(
                replacement['form'], tag.get_tag()
            )

            replacement_word = generated
            if isinstance(replacement_word, bool):
                replacement_word = replacement['form']

            if 'after_function' in replacement:
                replacement_word = replacement['after_function'](replacement_word)

            item['recommended_replace'] = replacement_word

        def mask_container(
                self,
                container,
                start_idx,
                replacing_mode: PersonalNameReplacingMode
                    = PersonalNameReplacingMode.STATIC,
                other_replacing_mode: OtherReplacingMode
                    = OtherReplacingMode.STATIC
        ):
            was_replaced = False
            for ne in container['entities']:
                if not was_replaced:
                    self.mask(ne, start_idx, replacing_mode, other_replacing_mode)
                    was_replaced = True
                else:
                    ne['recommended_replace'] = ''

    """
    Basic policies.
    """

    class CountryPolicy(BasicPolicy):
        """
        Country name masking policy.
        """

        def __init__(self, context, conf):
            super().__init__(context, conf, [
                        {
                            'form': 'Gibraltar',
                            'tag_template': 'NNIS1-----A----',
                            'prepositions': {
                                've': 'v',  # ve Francii vs. v Gibraltaru
                                'na': 'v',  # na Kypru vs. v Gibraltaru
                                'ze': 'z'   # ze Španělska vs. z Gibraltaru
                            }
                        },
                        {
                            'form': 'Francie',
                            'tag_template': 'NNFS1-----A----',
                            'prepositions': {
                                'v': 've',  # v Gibraltaru vs. ve Francii
                                'na': 've', # na Kypru vs. ve Francii
                                'ze': 'z'   # ze Španělska vs. z Francie
                            }
                        },
                        {
                            'form': 'Portoriko',
                            'tag_template': 'NNNS1-----A----',
                            'prepositions': {
                                've': 'v',  # ve Francii vs. v Portoriku
                                'na': 'v',  # na Kypru vs. v Portoriku
                                'ze': 'z'   # ze Španělska vs. z Portorika
                            }
                        },
                        {
                            'form': 'Zambie',
                            'tag_template': 'NNFS1-----A----',
                            'prepositions': {
                                've': 'v',  # ve Francii vs. v Zambii
                                'na': 'v',  # na Kypru vs. v Zambii
                                'z': 'ze'   # z Francie vs. ze Zambie
                            }
                        },
                    ])

    class TownPolicy(BasicPolicy):
        """
        Town name masking policy.
        """

        def __init__(self, context, conf):
            super().__init__(context, conf, [
                        {
                            'form': 'Plzeň',
                            'tag_template': 'NNFS1-----A----',
                            'prepositions': {
                                've': 'v'
                            }
                        },
                        {
                            'form': 'Vlašim',
                            'tag_template': 'NNFS1-----A----',
                            'prepositions': {
                                'v': 've'
                            }
                        },
                        {
                            'form': 'Dobříš',
                            'tag_template': 'NNFS1-----A----',
                            'prepositions': {
                                've': 'v'
                            }
                        },
                        {
                            'form': 'Olomouc',
                            'tag_template': 'NNFS1-----A----',
                            'prepositions': {
                                've': 'v'
                            }
                        },
                        {
                            'form': 'Liberec',
                            'tag_template': 'NNIS1-----A----',
                            'prepositions': {
                                've': 'v'
                            }
                        },
                        {
                            'form': 'Zlín',
                            'tag_template': 'NNIS1-----A----',
                            'prepositions': {
                                'v': 've'
                            }
                        },
                        {
                            'form': 'Opočno',
                            'tag_template': 'NNNS1-----A----',
                            'prepositions': {
                                've': 'v'
                            }
                        },
                        {
                            'form': 'Kladno',
                            'tag_template': 'NNNS1-----A----',
                            'prepositions': {
                                've': 'v'
                            }
                        }
                    ])

    class HydronymPolicy(BasicPolicy):
        """
        Hydronym masking policy.
        """

        def __init__(self, context, conf):
            super().__init__(context, conf, [
                        {
                            'form': 'Ohře',
                            'tag_template': 'NNFS1-----A----',
                            'prepositions': {
                                've': 'v'
                            }
                        },
                        {
                            'form': 'Sázava',
                            'tag_template': 'NNFS1-----A----',
                            'prepositions': {
                                've': 'v'
                            }
                        },
                        {
                            'form': 'Odra',
                            'tag_template': 'NNFS1-----A----',
                            'prepositions': {
                                've': 'v'
                            }
                        },
                        {
                            'form': 'Ploučnice',
                            'tag_template': 'NNFS1-----A----',
                            'prepositions': {
                                've': 'v'
                            }
                        },
                        # TODO: Labe is the ONLY river of gender neutrum -> we will not pseudonymize it
                        {
                            'form': 'Labe',
                            'tag_template': 'NNNS1-----A----',
                            'prepositions': {
                                've': 'v'
                            }
                        }
                    ])

    class StreetSquarePolicy(BasicPolicy):
        """
        Street/Square masking policy.
        """

        def __init__(self, context, conf):
            super().__init__(context, conf, [
                {
                    'form': 'Tržní',
                    'tag_template': 'NNIS1-----A----',
                    'prepositions': {
                        'v': 'na',
                        've': 'na'
                    },
                    'after_function': lambda s: s.capitalize()
                },
                {
                    'form': 'Panská',
                    'tag_template': 'NNFS1-----A----',
                    'after_function': lambda s: s.capitalize(),
                    'prepositions': {
                        'v': 'na',
                        've': 'na',
                        'na': 'v'
                    }
                },
                {
                    'form': 'Nádraží',
                    'tag_template': 'NNNS1-----A----',
                    'after_function': lambda s: s.capitalize(),
                    'prepositions': {
                        'v': 'na',
                        've': 'na'
                    }
                },
                {
                    'form': 'Sedliště',
                    'tag_template': 'NNNS1-----A----',
                    'after_function': lambda s: s.capitalize(),
                    'prepositions': {
                        'v': 'na',
                        've': 'na'
                    }
                }
            ])

    class CompanyOrganizationPolicy(BasicPolicy):
        """
        Company, organization, etc. masking policy.
        """

        def __init__(self, context, conf):
            super().__init__(context, conf, [
                {
                    'form': 'Výdělečná',
                    'tag_template': 'NNIPX-----A----',
                    'after_function': lambda s: s.capitalize()
                },
                {
                    'form': 'Bezkonkurenční',
                    'tag_template': 'NNNPX-----A----',
                    'after_function': lambda s: s.capitalize()
                },
                {
                    'form': 'Flexibilní',
                    'tag_template': 'NNFPX-----A----',
                    'after_function': lambda s: s.capitalize()
                }
            ])

    class CultEducSciPolicy(BasicPolicy):
        """
        Cult., Educ., Sci., etc. masking policy.
        """

        def __init__(self, context, conf):
            super().__init__(context, conf, [
                {
                    'form': 'Věž',
                    'tag_template': 'NNFSX-----A----',
                    'after_function': lambda s: s.capitalize()
                },
                {
                    'form': 'Hrad',
                    'tag_template': 'NNMSX-----A----',
                    'after_function': lambda s: s.capitalize()
                },
                {
                    'form': 'Divadlo',
                    'tag_template': 'NNNSX-----A----',
                    'after_function': lambda s: s.capitalize()
                }
            ])

    class ArtifactPolicy(BasicPolicy):
        """
        'o' - artifact names (cultural artifacts (books, movies), ...)
            o_ - underspecified
            oa - cultural artifacts (books, movies)
            op - products
            or - directives/norms
        """

        def __init__(self, context, conf):
            super().__init__(context, conf, [
                {
                    'form': 'Babička',
                    'tag_template': 'NNFSX-----A----',
                    'after_function': lambda s: s.capitalize()
                },
                {
                    'form': 'Dědeček',
                    'tag_template': 'NNMSX-----A----',
                    'after_function': lambda s: s.capitalize()
                },
                {
                    'form': 'Zvířátko',
                    'tag_template': 'NNNSX-----A----',
                    'after_function': lambda s: s.capitalize()
                }
            ])

    class PeriodicalPolicy(BasicPolicy):
        """
        'mn' - periodical
        """

        def __init__(self, context, conf):
            super().__init__(context, conf, [
                {
                    'form': 'Novina',
                    'tag_template': 'NNFSX-----A----',
                    'after_function': lambda s: s.capitalize()
                },
                {
                    'form': 'Žurnál',
                    'tag_template': 'NNISX-----A----',
                    'after_function': lambda s: s.capitalize()
                },
                {
                    'form': 'Infy',
                    'tag_template': 'NNNSX-----A----',
                    'after_function': lambda s: s.capitalize()
                }
            ])


    """
    Non-basic policies.
    """

    class NamePolicy(NERMaskingPolicy):
        """
        Personal name masking policy.
        """
        # identity mapping
        # i.e. key is [lemma(what_was_encountered)...]
        # value is index to the corresponding array with identity
        m = {}
        m_pf_count = 0
        f_pf_count = 0

        m_ps_count = 0
        f_ps_count = 0

        def __init__(self, context, conf):
            """
            :param context:
            :param conf:
            """
            super().__init__(context, conf)

            # sometimes surname is adjective, e.g. Bohuslav Černý, where Černý is adjective whereas
            # Pavel Dvořák, where Dvořák is a noun.
            self.ps_options = {
                # M = Masculinum
                'M': {
                    'pf': [
                        {
                            'N': 'Jan',
                            'A': 'Janovu'
                        },
                        {
                            'N': 'Karel',
                            'A': 'Karlovu'
                        },
                        {
                            'N': 'Franta',
                            'A': 'Frantovu'
                        },
                        {
                            'N': 'Jindřich',
                            'A': 'Jindřichovu'
                        },
                        {
                            'N': 'Bedřich',
                            'A': 'Bedřichovu'
                        },
                        {
                            'N': 'Antonín',
                            'A': 'Antonínovu'
                        }
                    ],
                    'ps': [
                        {
                            'N': 'Dvořák',
                            'A': 'Dvořákovu'
                        },
                        {
                            'N': 'Panák',
                            'A': 'Panákovu'
                        },
                        {
                            'N': 'Myslivec',
                            'A': 'Myslivcovu'
                        },
                        {
                            'N': 'Kovář',
                            'A': 'Kovářovo'
                        },
                        {
                            'N': 'Smetana',
                            'A': 'Smetanovo'
                        },
                        {
                            'N': 'Sedlák',
                            'A': 'Sedlákovo'
                        },
                    ]
                },
                # F = Femininum
                'F': {
                    'pf': [
                        {
                            'N': 'Jana',
                            'A': 'Janino'
                        },
                        {
                            'N': 'Hana',
                            'A': 'Hanino'
                        },
                        {
                            'N': 'Anna',
                            'A': 'Annino'
                        },
                        {
                            'N': 'Marie',
                            'A': 'Mariino'
                        }
                    ],
                    'ps': [
                        {
                            'N': 'Nováková',
                            'A': 'Novákové'
                        },
                        {
                            'N': 'Dvořáková',
                            'A': 'Dvořákové'
                        },
                        {
                            'N': 'Veselá',
                            'A': 'Veselé'
                        },
                        {
                            'N': 'Horáková',
                            'A': 'Horákové'
                        }
                    ]
                }
            }

        def mask(
            self,
            item,
            start_idx,
            replacing_mode: PersonalNameReplacingMode =
                PersonalNameReplacingMode.STATIC,
            other_replacing_mode: OtherReplacingMode =
                OtherReplacingMode.STATIC
        ):
            """
            How to mask the first name of surname.
            :param other_replacing_mode:
            :param item:
            :param start_idx:
            :param replacing_mode:
            :return:
            """

            def make_tag_defaulting(tag):
                tag = tag.change_variant('-')

                # masculine inanimate => default to masculine (animate)
                if tag.gender() == 'I':
                    tag = tag.change_gender('M')

                # name should be either masculinum or femininum
                # MorphoDiTa might say it is 'X' or 'N' (neutrum)
                # if it is the case, default to masculinum.
                if tag.gender() not in ['M', 'F'] and \
                   tag.possesors_gender() not in ['M', 'F']:
                    # defaulting
                    tag = tag.change_gender('M')
                    tag = tag.change_possesors_gender('M')

                # grammatical case is not known (between 1 and 7)
                # default to 1st case.
                if tag.case() not in [str(case) for case in range(1, 7 + 1)]:
                    tag = tag.change_case('1')

                return tag

            def get_replacement_index():
                """
                Get index to the named entity replacement if it was not registered.
                :return:
                """
                nonlocal item
                entity_type = item['entity_type']

                MASCULINE_NAME_COUNT = len(self.ps_options['M'][entity_type])
                FEMININE_NAME_COUNT = len(self.ps_options['F'][entity_type])

                index = 0
                if replacing_mode == PersonalNameReplacingMode.STATIC:
                    # static mode => index is always zero
                    index = 0
                elif replacing_mode == PersonalNameReplacingMode.RANDOM:
                    # random mode => index is random with bounds given by gender
                    if TagWrapper.get_replacement_gender(tag) == 'M':
                        index = random.randint(0, MASCULINE_NAME_COUNT - 1)
                    if TagWrapper.get_replacement_gender(tag) == 'F':
                        index = random.randint(0, FEMININE_NAME_COUNT - 1)
                elif replacing_mode == PersonalNameReplacingMode.CONTEXT:
                    # context mode => consider we did not register this name
                    if TagWrapper.get_replacement_gender(tag) == 'M':
                        if entity_type == 'pf':
                            index = self.m_pf_count % MASCULINE_NAME_COUNT
                        elif entity_type == 'ps':
                            index = self.m_ps_count % MASCULINE_NAME_COUNT
                    if TagWrapper.get_replacement_gender(tag) == 'F':
                        if entity_type == 'pf':
                            index = self.f_pf_count % FEMININE_NAME_COUNT
                        elif entity_type == 'ps':
                            index = self.f_ps_count % FEMININE_NAME_COUNT

                return index

            def context_registration(tag):
                nonlocal item
                lemma = item['lemma']
                entity_type = item['entity_type']

                index = get_replacement_index()

                if TagWrapper.get_replacement_gender(tag) == 'M':
                    identity = self.ps_options['M'][entity_type][index]
                    CSPolicy.NamePolicy.m[lemma.capitalize()] = identity

                    if entity_type == 'pf':
                        CSPolicy.NamePolicy.m_pf_count += 1
                    elif entity_type == 'ps':
                        CSPolicy.NamePolicy.m_ps_count += 1
                elif TagWrapper.get_replacement_gender(tag) == 'F':
                    identity = self.ps_options['F'][entity_type][index]
                    CSPolicy.NamePolicy.m[lemma.capitalize()] = identity

                    if entity_type == 'pf':
                        CSPolicy.NamePolicy.f_pf_count += 1
                    elif entity_type == 'ps':
                        CSPolicy.NamePolicy.f_ps_count += 1

            def preserve_capitalization(template, other):
                if template == template.capitalize():
                    return other.capitalize()

                return other

            def solve_prepositions():
                nonlocal item
                tok_idx = item['token_idx']
                if 0 <= tok_idx - 1 < EntityRecognition.get_ner_repr_length(self.context) and \
                        'form' in EntityRecognition.get_nth_item(tok_idx - 1, self.context):
                    previous = EntityRecognition.get_nth_item(
                            tok_idx - 1,
                            self.context
                    )['form']

                    # consider 'se Smetanou' vs. 's Myslivcem'.
                    if previous.lower() in ['s', 'se'] \
                       and tag.case() == '7':
                        # names beginning with 'S' with 's' before it should be replaced to 'se'
                        if to_replace[0] == 'S':
                            EntityRecognition.get_nth_item(
                                tok_idx - 1,
                                self.context
                            )['recommended_replace'] = \
                                preserve_capitalization(previous, 'se')
                        else:
                            EntityRecognition.get_nth_item(
                                tok_idx - 1,
                                self.context
                            )['recommended_replace'] = \
                                preserve_capitalization(previous, 's')

                    # consider 'k Janovi' vs. 'ke Karlovi'.
                    elif previous.lower() in ['k', 'ke']:
                        if to_replace[0] == 'K':
                            EntityRecognition.get_nth_item(
                                tok_idx - 1,
                                self.context
                            )['recommended_replace'] = \
                                preserve_capitalization(previous, 'ke')
                        else:
                            EntityRecognition.get_nth_item(
                                tok_idx - 1,
                                self.context
                            )['recommended_replace'] = \
                                preserve_capitalization(previous, 'k')

            lemma = item['lemma']
            entity_type = item['entity_type']
            tag = TagWrapper(item['tag'])

            # it is entity that we do not want to pseudonymize
            if entity_type not in self.conf:
                return

            # bad part of speach (i.e. not noun nor adjective)
            if tag.part_of_speach() not in ['N', 'A']:
                return

            tag = make_tag_defaulting(tag)

            #####################
            # Make a registration
            #####################

            # gender for casing
            replacing_gender = TagWrapper.get_replacement_gender(tag)

            # not a context mode
            if replacing_mode != PersonalNameReplacingMode.CONTEXT:
                CSPolicy.NamePolicy.m[lemma.capitalize()] =\
                    self.ps_options[replacing_gender][entity_type][get_replacement_index()]
            # not registered and we are in context mode ==> we should register
            elif lemma.capitalize() not in CSPolicy.NamePolicy.m:
                context_registration(tag)

            ####################
            # Create replacement
            ####################

            # if is possesive adjective, then we need to replace by adjective form
            if tag.possesors_gender() != '-':
                to_replace = CSPolicy.NamePolicy.m[lemma.capitalize()]['A']
            # not possesive -> we'll not solve other cases, if exist
            else:
                # not possesive but still adjective -> convert tag to noun form
                if tag.part_of_speach() == 'A':
                    tag = tag.change_part_of_speach('N') \
                        .change_sub_pos('N') \
                        .change_degree_of_comparison('-')

                to_replace = CSPolicy.NamePolicy.m[lemma.capitalize()]['N']

            gen = mg.MW.generate(to_replace, tag.get_tag())
            if isinstance(gen, bool):
                log.warning(
                    f'Could not generate proper form. '
                    f'Lemma was: {lemma}, '
                    f'tag was: {tag.get_tag()}, '
                    f'to replace was: {to_replace}'
                )
                item['recommended_replace'] = to_replace.capitalize()
            else:
                item['recommended_replace'] = gen.capitalize()

            solve_prepositions()

        def mask_container(
                self,
                container,
                start_idx,
                replacing_mode: PersonalNameReplacingMode =
                    PersonalNameReplacingMode.STATIC,
                other_replacing_mode: OtherReplacingMode =
                    OtherReplacingMode.STATIC
        ):
            """
            How to mask container with first name and second name.
            Container has field 'entities', which is array of entities.
            So to know if the object is container, we just find out if it has field 'entities'.
            :param other_replacing_mode:
            :param replacing_mode:
            :param container: named entity container
            :return: masked named entity container
            """
            surname_encountered = False
            firstname_encountered = False

            ne_iter = iter(range(len(container['entities'])))
            for ne_idx in ne_iter:
                # entity inside 'P' or 'ps' container
                ne = container['entities'][ne_idx]

                if 'entity_type' in ne and ne['entity_type'] not in self.conf:
                    start_idx += 1
                    continue

                # R=Preposition = might be the case of preposition in names
                # like 'John von Neumann' where 'von' is preposition it does not have gender.
                # then the 'main' surname should follow
                if TagWrapper(ne['tag']).part_of_speach() == 'R' and\
                   0 <= ne_idx + 1 < len(container['entities']):
                    ne['recommended_replace'] = ''
                    ne = container['entities'][ne_idx + 1]
                    ne['entity_type'] = 'ps'
                    start_idx += 1
                    next(ne_iter)
                else:
                    # not a preposition

                    # not a named entity => don't pseudonymize
                    if not EntityRecognition.is_simple_entity(ne):
                        ne['recommended_replace'] = ''
                        start_idx += 1
                        continue

                    # its firstname or surname, but we have already encountered one
                    if (firstname_encountered and ne['entity_type'] == 'pf') or \
                       (surname_encountered and ne['entity_type'] == 'ps'):
                        ne['recommended_replace'] = ''
                        start_idx += 1
                        continue

                    # middle name, let's ignore it
                    if ne['entity_type'] == 'pm':
                        ne['recommended_replace'] = ''
                        start_idx += 1
                        continue

                    # its surename and we have not encountered one yet
                    if ne['entity_type'] == 'ps':
                        surname_encountered = True

                    if ne['entity_type'] == 'pf':
                        firstname_encountered = True

                # cases are: it is (first encountered) firstname or
                # (first encountered) surname.
                # let's pseudonymize it, iff user wants it.

                if ne['entity_type'] in ['pf', 'ps']:
                    self.mask(ne, start_idx, replacing_mode, other_replacing_mode)

                start_idx += 1

    class TimeExpression(NERMaskingPolicy):
        """
        Complex Time Expression masking policy.
        It is probably better to use custom NERMaskingPolicy than BasicPolicy
        """
        def __init__(self, context, conf):
            super().__init__(context, conf)

        def mask(
            self,
            item,
            start_idx,
            replacing_mode: PersonalNameReplacingMode = PersonalNameReplacingMode.STATIC,
            other_replacing_mode: OtherReplacingMode = OtherReplacingMode.STATIC
        ):
            """

            :param other_replacing_mode:
            :param item:
            :param start_idx:
            :param replacing_mode:
            :return:
            """

        def mask_container(
            self,
            container,
            start_idx,
            replacing_mode: PersonalNameReplacingMode = PersonalNameReplacingMode.STATIC,
            other_replacing_mode: OtherReplacingMode = OtherReplacingMode.STATIC
        ):
            for ne in container['entities']:
                if 'entity_type' not in ne:
                    continue

                et = ne['entity_type']
                # days
                if et == 'td':
                    # NameTag 2 makes '15. února' =>
                    #   '<ne type='td'><token>15</token><token>.</token></ne>'.
                    # we don't want to consider '.' as a day.
                    if ne['form'] == '.':
                        continue

                    if other_replacing_mode == OtherReplacingMode.RANDOM:
                        ne['recommended_replace'] = str(random.randint(1, 31))
                    elif other_replacing_mode == OtherReplacingMode.STATIC:
                        ne['recommended_replace'] = '15'
                # months
                elif et == 'tm':
                    if ne['form'] == '.':
                        continue

                    # check if month is written as number
                    if re.match('\\d+', ne['form']):
                        if other_replacing_mode == OtherReplacingMode.RANDOM:
                            ne['recommended_replace'] = str(random.randint(1, 12))
                        elif other_replacing_mode == OtherReplacingMode.STATIC:
                            ne['recommended_replace'] = '4'
                    # month is given as a word
                    else:
                        # try to use the correct case
                        # we will replace with 'říjen' only
                        # 'říjen' has gender of masculine inanimate
                        tag = TagWrapper(ne['tag'])\
                              .change_gender('I')\
                              .change_variant('-')\
                              .change_part_of_speach('N')
                        to_replace = mg.MW.generate(
                            'říjen', tag.get_tag()
                        )
                        if not isinstance(to_replace, bool):
                            ne['recommended_replace'] = to_replace
                        # morphological generation went wrong, so just replace with 'říjen'
                        else:
                            ne['recommended_replace'] = 'říjen'
                # years
                elif et == 'ty':
                    if ne['form'] == '.':
                        continue

                    ne['recommended_replace'] = str(random.randint(100, 2100))
