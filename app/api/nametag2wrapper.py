"""
NameTag 2 Wrappers
"""
from typing import List
import xml.etree.ElementTree as ET
import requests

from entityrecognition import EntityRecognition
import mg


class NameTag2Wrapper(EntityRecognition):
    """
    NameTag 2 Wrappers
    """
    def serialize(self, text: str, lang: str) -> List[dict]:
        """
        Perform named entity recognition in the text. Use NameTag 2 (by API).
        :return: array of JSONs that identifies each named entity &
                 other entities (non-named entities, ...)
        """
        tags = mg.MW.tag_text(text)
        tokens = mg.MW.flatten_tokenize(text)
        just_tokens = list(map(lambda token: token['token'], tokens))
        vertical_text = '\n'.join(just_tokens)

        rest_api_model = {
            'cs': 'czech-cnec2.0-200831',
            'en': 'english-conll-200831'
        }

        selected_model = rest_api_model[lang]

        resp_dict = requests.get(
            f'http://lindat.mff.cuni.cz/services/nametag/api/recognize?input=vertical&model={selected_model}&data=' \
                + requests.utils.quote(vertical_text)
        ).json()

        result = resp_dict['result']

        # ET needs exactly one top level document
        recognized_xml = '<root>' + result + '</root>'

        tree = ET.ElementTree(ET.fromstring(recognized_xml))
        root = tree.getroot()

        token_counter = 0
        prev_token_end = 0
        items = []

        for sentence in root:
            for child in sentence:
                # container of named entities
                # I.e. it has more than one <token>/<ne> at the start OR
                # one of the children has more than one <token>/<ne>.
                if len(child) > 1 or\
                   any(
                       len(child_of_child) > 1
                            for child_of_child in child.iter('*')
                   ):
                    cur_container = {}

                    cur_container['container_type'] = child.attrib['type']
                    cur_container['range'] = {
                        'start': token_counter
                    }

                    cur_container['entities'] = []

                    prev_ne_type = None
                    # iterates even over the container element!!!
                    # so <ne type="P">
                    #       <token>President</token>
                    #       <ne type="pf">
                    #           <token>Barack</token>
                    #       </ne>
                    #       <ne type="ps">
                    #           <token>Obama</token>
                    #       </ne>
                    #    </ne>
                    # will first take the <ne type="P">...</ne>.
                    for child_of_child in child.iter('*'):
                        # it is <token>some_text</token>
                        if len(child_of_child) == 0:
                            cur_entity = {
                                'form': child_of_child.text,
                                'token_idx': token_counter,
                                'tag': tags[token_counter]['tag'],
                                'lemma': tags[token_counter]['lemma'],
                                'pre_whitespaces':
                                    text[prev_token_end: tokens[token_counter]['start']]
                            }

                            if prev_ne_type:
                                cur_entity['entity_type'] = prev_ne_type

                            cur_container['entities'].append(cur_entity)
                            prev_token_end = tokens[token_counter]['end']
                            token_counter += 1

                        # remember parent type, i.e.: <ne type="THIS"><token>some_text</token></ne>
                        # 'P' is not a desired parent. It is strictly a container.
                        prev_ne_type = child_of_child.attrib['type']\
                            if 'type' in child_of_child.attrib and\
                               child_of_child.attrib['type'] not in ['P', 'PER'] else None

                    # 0 indexing
                    cur_container['range']['end'] = token_counter - 1
                    items.append(cur_container)
                # single token (not named entity), i.e. <token>some_text</token>
                elif len(child) == 0:
                    entity = {
                        'form': child.text,
                        'tag': tags[token_counter]['tag'],
                        'pre_whitespaces':
                            text[prev_token_end: tokens[token_counter]['start']]
                    }
                    items.append(entity)
                    prev_token_end = tokens[token_counter]['end']
                    token_counter += 1
                # single named entity, maybe nested a lot
                #   (e.g. '<ne type="ia"><ne type="gc"><token>Normandy</token></ne></ne>')
                # i.e. last <token> has length 0, and all the other <ne>'s has length 1.
                elif all(
                        len(child_of_child) in [0, 1]
                        for child_of_child in child.iter('*')
                ):
                    last_ne_type = child.attrib['type']
                    token_text = ''
                    for child_of_child in child.iter('*'):
                        if len(child_of_child) == 1:
                            last_ne_type = child_of_child.attrib['type']
                        else:
                            token_text = child_of_child.text

                    entity = {
                        'form': token_text,
                        'token_idx': token_counter,
                        'entity_type': last_ne_type,
                        'tag': tags[token_counter]['tag'],
                        'lemma': tags[token_counter]['lemma'],
                        'pre_whitespaces':
                            text[prev_token_end: tokens[token_counter]['start']]
                    }
                    items.append(entity)
                    prev_token_end = tokens[token_counter]['end']
                    token_counter += 1
                else:
                    # log the error
                    self.log.error(f'Unexpected path in nametag2wrapper.py... It was {ET.tostring(child)}')
                    raise Exception('Unexpected path in nametag2wrapper.py...')

        return items
