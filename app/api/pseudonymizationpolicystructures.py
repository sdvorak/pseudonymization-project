"""
Enums for pseudonymization options.
"""

from enum import Enum


class PersonalNameReplacingMode(Enum):
    """
    STATIC = e.g. all the first names will be replaced with the same name
    RANDOM = e.g. all the first names will be replaced with random name
    CONTEXT = e.g. all surnames "Novotný" will be replaced with consistent surname
    """
    RANDOM = 'random'
    CONTEXT = 'context'
    STATIC = 'static'


class OtherReplacingMode(Enum):
    """
    STATIC = e.g. all the first names will be replaced with the same name
    RANDOM = e.g. all the first names will be replaced with random name
    """
    RANDOM = 'random'
    STATIC = 'static'
