# Pseudonymization project

[[_TOC_]]

## About
Tool for pseudonymization of Czech and English text according to GDPR's definition. Moreover, personal information is replaced using semantic and syntactic equivalents. The project was created as an individual software project at MFF. Front end was created in React, and the API in Python (Flask). A tool named MorphoDiTa was used for the morphological generation, and NameTag 2 for Named Entity Recognition.


## Install and run in Docker
*(or other containerization platform, e.g. podman with podman-compose)*

```shell
# get the project, e.g.
git clone git@gitlab.mff.cuni.cz:dvoraks8/rocnikovy-projekt.git
```

Do not forget to download models. Instructions are [here](app/api/README.md).

```bash
# go to app/ folder
cd rocnikovy-projekt/app/
# build container
docker-compose build
# run container
docker-compose up
```


## Tech stack
Frontend
- [React](https://reactjs.org/)
- [React Bootstrap](https://react-bootstrap.github.io/)

API
- [Flask](https://flask.palletsprojects.com/en/2.1.x/)
- [Swagger](https://swagger.io/) (API specification)
