# Useful links
- [About tags](https://ufal.mff.cuni.cz/pdt2.0/doc/manuals/en/m-layer/html/ch02s02s01.html)
- [About entities recognition notation](https://ufal.mff.cuni.cz/cnec)


# Terminology (Czech)
- *form* = vyskloňované slovo
- *lemma* = slovo v základním tvaru (pro podstatné jméno např. 1. pád, číslo jednotné)


# NLP services

## MorphoDiTa
- [MorphoDiTa page](https://ufal.mff.cuni.cz/morphodita)
- [MorphoDiTa GitHub](https://github.com/ufal/morphodita)
- [MorphoDiTa web service](http://lindat.mff.cuni.cz/services/morphodita/run.php)
- [MorphoDiTa PyPI](https://pypi.org/project/ufal.morphodita/)

## NameTag 1
- [NameTag 1 page](https://ufal.mff.cuni.cz/nametag/1)
- [NameTag 1 GitHub](https://github.com/ufal/nametag)
- [NameTag 2 web service](http://lindat.mff.cuni.cz/services/nametag/)
- [NameTag 1 PyPI](https://pypi.org/project/ufal.nametag/)
