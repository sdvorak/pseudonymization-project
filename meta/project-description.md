# Project description

## In Czech

Pseudonymizace textových dokumentů

Pseudonymizace textových dokumentů je proces, při němž je vstupní text změněn tak, aby neobsahoval osobní informace (např. jména, adresy, rodná čísla, tel. čísla, emailové adresy, datumy, apod.). Po takové úpravě již není text jazykově korektní a nelze ho použít např. pro trénování modelů zpracování přirozeného jazyka. 


Cílem ročníkového projektu je implementovat systém pro tzv. pseudonymizaci textových dokumentů a zprovoznit jej webovou službu. Systém bude využívat existující nástroje pro lingvistické zpracování textů (jako je rozpoznávání pojmenovaných entit a morfologická analýza a syntéza). Ve vstupním textu budou automaticky identifikovány osobní informace, které budou následně buď jen označeny a případně odstraněny, nebo nahrazeny jinými, tak aby výsledný text zůstal z jazykového hlediska korektní.  

Součástí práce bude i implementace REST API a webového rozhraní, které bude zprovozněno v infrastruktuře LINDAT. Systém bude podporovat češtinu a angličtinu, případně další jazyky. 


## Front-end workflow (proposal)

1. User types/pastes his text to be pseudonymized. Also, user selects all the named entity types to be pseudonymized (e.g. first names, surnames, addresses).
2. When user is done with configuration, user clicks preview button.
3. App will show pseudonymized text. Pseudonymized entities will be bold.
4. User can click on the text token, there are multiple scenarios. User clicked
    - token not subject of NER,
        - user can change it to other token
    - standalone named entity token (e.g. `Karel`).
        - user can change the word that is being used as a pseudonymization placeholder (e.g. app will pseudonymize `Karel je kamarád.` to `Honza je kamarád`, then user can change `Honza` to `Franta`).
    - container named entity (e.g. `Karel Novák` is container of first name and surname). User can ONLY change the whole container, except
        - in case of first name and last name container user will be provided with option to change either first name and/or last name. Then all the other matching words will be replaced as well, NO MATTER of the semantic connection.
        - in future, maybe some container will have its specialities as well
                




